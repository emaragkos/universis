import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {PreviewsRoutingModule} from './previews.routing';
import {GraduationRulesComponent} from './components/graduation-rules/graduation-rules.component';
import { GraduationProgressComponent } from './components/graduation-progress/graduation-progress.component';
import { GraduationSimplifiedComponent } from './components/graduation-simplified/graduation-simplified.component';

@NgModule({
  imports: [
    HighlightIncludeModule,
    PreviewsRoutingModule
  ],
  declarations: [
    GraduationRulesComponent,
    GraduationProgressComponent,
    GraduationSimplifiedComponent,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class PreviewsModule {}

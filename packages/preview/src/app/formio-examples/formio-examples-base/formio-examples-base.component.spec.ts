import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormioExamplesBaseComponent } from './formio-examples-base.component';

describe('FormioExamplesBaseComponent', () => {
  let component: FormioExamplesBaseComponent;
  let fixture: ComponentFixture<FormioExamplesBaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormioExamplesBaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormioExamplesBaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

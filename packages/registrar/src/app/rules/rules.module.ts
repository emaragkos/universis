import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MostModule} from '@themost/angular';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {RuleService} from './services/rule.service';
import {ItemRulesComponent, ItemRulesModalComponent} from './components/item-rules/item-rules.component';
import {DEFAULT_RULES_CONFIG, RuleConfiguration, RULES_CONFIG} from './services/rule.configuration';
import {RouterModalModule} from '@universis/common/routing';
import {AdvancedFormsModule} from '@universis/forms';
import { NgArrayPipesModule } from 'ngx-pipes';
import {CalculationRulesComponent, CalculationRulesModalComponent} from './components/calculation-rules/calculation-rules.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TranslateModule,
    MostModule,
    RouterModalModule,
    AdvancedFormsModule,
    NgArrayPipesModule
  ],
  declarations: [
    ItemRulesComponent,
    ItemRulesModalComponent,
    CalculationRulesComponent,
    CalculationRulesModalComponent
  ],
  exports: [
    ItemRulesComponent,
    ItemRulesModalComponent,
    CalculationRulesComponent,
    CalculationRulesModalComponent
  ]
})
export class RulesModule {

  static forRoot(configuration?: RuleConfiguration): ModuleWithProviders {
    return {
      ngModule: RulesModule,
      providers: [
        {
          provide: RULES_CONFIG,
          useValue: configuration || DEFAULT_RULES_CONFIG
        },
        RuleService
      ]
    };
  }
  constructor(@Optional() @SkipSelf() parentModule: RulesModule,
              private _translateService: TranslateService) {
    this.ngOnInit().catch( err => {
      console.error('An error occurred while loading RulesModule');
      console.error(err);
    });
  }

  // tslint:disable-next-line:use-life-cycle-interface
  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/rules.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }
}

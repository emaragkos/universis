import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {ItemRule, RuleService} from '../../services/rule.service';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {OnDestroy} from '@angular/core/src/metadata/lifecycle_hooks';
import {Subscription} from 'rxjs/Subscription';
import {ActivatedRouteDataPreProcessor, AdvancedFormComponent, AdvancedFormModalData, ServiceUrlPreProcessor} from '@universis/forms';
import {AngularDataContext} from '@themost/angular';
import * as StudentRuleForm from '../../assets/forms/StudentRule.json';
import * as CourseTypeRuleForm from '../../assets/forms/CourseTypeRule.json';
import * as CourseAreaRuleForm from '../../assets/forms/CourseAreaRule.json';
import * as CourseSectorRuleForm from '../../assets/forms/CourseSectorRule.json';
import * as CourseRuleForm from '../../assets/forms/CourseRule.json';
import * as RegisteredCourseRuleForm from '../../assets/forms/RegisteredCourseRule.json';
import * as ThesisRuleForm from '../../assets/forms/ThesisRule.json';
import * as InternshipRuleForm from '../../assets/forms/InternshipRule.json';
import * as YearMeanGradeRuleForm from '../../assets/forms/YearMeanGradeRule.json';
import * as MeanGradeRuleForm from '../../assets/forms/MeanGradeRule.json';
import * as NotSupportedRule from '../../assets/forms/NotSupportedRule.json';
import * as ProgramGroupRuleForm from '../../assets/forms/ProgramGroupRule.json';
import * as CourseCategoryRuleForm from '../../assets/forms/CourseCategoryRule.json';
import {DataModelRule} from '../../services/rule.configuration';
import {ErrorService} from '@universis/common';
import * as ts from 'tsickle/src/typescript-2.4';
import typeElement = ts.ScriptElementKind.typeElement;
import {TranslateService} from '@ngx-translate/core';


export interface RuleFormModalData extends AdvancedFormModalData {
  navigationProperty: string;
}

@Component({
  selector: 'app-item-rules',
  templateUrl: './item-rules.component.html',
  styleUrls: ['./item-rules.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None
})
export class ItemRulesComponent implements OnInit, OnChanges {

  components: Map<string, any> = new Map([
    [ 'StudentRule', StudentRuleForm],
    [ 'CourseRule', CourseRuleForm],
    [ 'CourseTypeRule', CourseTypeRuleForm],
    [ 'CourseAreaRule', CourseAreaRuleForm],
    [ 'CourseSectorRule', CourseSectorRuleForm],
    [ 'RegisteredCourseRule', RegisteredCourseRuleForm],
    [ 'ThesisRule', ThesisRuleForm],
    [ 'InternshipRule', InternshipRuleForm],
    [ 'YearMeanGradeRule', YearMeanGradeRuleForm],
    [ 'MeanGradeRule', MeanGradeRuleForm],
    [ 'ProgramGroupRule', ProgramGroupRuleForm],
    [ 'CourseCategoryRule', CourseCategoryRuleForm],

  ]);

  constructor(private _rules: RuleService,
              private _context: AngularDataContext,
              private _injector: Injector) {
    //
  }

  @Input() items: {
    form: any;
    data: ItemRule
  }[] = [];

  public configuration: DataModelRule;

  @Input() target: any;
  @Input() entitySet: string;
  @Input() navigationProperty: string;

  ngOnInit() {

  }

  copyForm(srcForm: any): any {
    if (srcForm == null) {
      return null;
    }
    return JSON.parse(JSON.stringify(srcForm));
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (Object.prototype.hasOwnProperty.call(changes, 'target')) {
      if (changes.target != null) {
        // get rules
        this._rules.get(this.entitySet, this.target.id, this.navigationProperty).getItems().then((results) => {
          // get available children
          this.configuration = this._rules.getConfiguration(this.entitySet, this.navigationProperty);
          this.items = results.map((result) => {
            let formConfig = this.components.get(`${result.refersTo}Rule`);
            if (formConfig == null) {
              formConfig = NotSupportedRule;
            }
            // copy form
            const formCopy = this.copyForm(formConfig);
            // run pre-processors
            new ServiceUrlPreProcessor(this._context).parse(formCopy);
            // flat ruleOperator
            if (typeof result.ruleOperator === 'object') {
              result.ruleOperator = result.ruleOperator.id;
            }
            result = Object.assign(result, { 'target': this.target});
            return {
              form: formCopy,
              data: result
            };
          });
        });
      } else {
        this.items = [];
      }
    }
  }

  save(): Promise<any> {
    const items = this.items.map((item) => {
      if ((<any>(item.data)).remove) {
        // set state property
        Object.assign(item.data, {
          $state: 4
        });
      }
      return item.data;
    });
    return this._rules.get(this.entitySet, this.target.id, this.navigationProperty).save(items);
  }

  add(event: MouseEvent, componentType: string) {
    event.preventDefault();
    // copy form
    const formCopy = this.copyForm(this.components.get(componentType));
    // run pre-processors
    new ServiceUrlPreProcessor(this._context).parse(formCopy);
    const newItem = {
      form: formCopy,
      data: <ItemRule>{
        target: this.target,
        refersTo: componentType.replace(/Rule$/, '')
      }
    };
    this.items.unshift(newItem);
  }

  onCustomEvent(event, advancedForm: AdvancedFormComponent, index: number) {
    if (event.type === 'remove') {
      const findItem = this.items[index];
      if (findItem) {
        // if item is going to be removed
        if (findItem.data.id) {
          // update collection item
          Object.assign(findItem.data, event.data, {
            $state: 4 // and set state for delete
          });
        } else {
          // else if item is new remove it from collection
          this.items.splice(index, 1);
        }
      }
    }
  }

}

@Component({
  selector: 'app-item-rules-modal',
  template: `
  <app-item-rules #itemRules ></app-item-rules>
  `,
})
export class ItemRulesModalComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @ViewChild('itemRules') itemRulesComponent: ItemRulesComponent;
  private subscription: Subscription;
  // dynamically set and handle queries for different rule attributes with the help of a Map
  // TODO: Investigate why searching values of these dropdowns does not work. Disable search temporarily.
  public dropdownDefinitions: Map<String, {url: string, label: string}> = new Map([
    ['studentStatus', {url: 'StudentStatuses?$orderby=name&$select=id, name&$top=-1', label: 'Status'}],
    ['category', {url: 'StudentCategories?$orderby=name&$select=id, name&$top=-1', label: 'Student Category'}],
    ['department', {url: 'LocalDepartments?$select=id, name&$orderby=name&$top=-1', label: 'Department'}],
    ['inscriptionSemester', {url: 'Semesters?$orderby=id&$select=id, name&$top=-1', label: 'Inscription Semester'}],
    ['inscriptionMode', {url: 'InscriptionModes?$orderby=name&$select=id, name&$top=-1', label: 'Inscription Mode'}],
    ['inscriptionPeriod', {url: 'AcademicPeriods?$orderby=id&$select=id, name&$top=-1', label: 'Inscription Period'}],
    ['inscriptionYear', {url: 'AcademicYears?$orderby=id desc&$select=id, name&$top=-1', label: 'Inscription Year'}],
    ['graduationPeriod', {url: 'AcademicPeriods?$orderby=id&$select=id, name&$top=-1', label: 'Graduation Period'}],
    ['graduationYear', {url: 'AcademicYears?$orderby=id desc&$select=id, name&$top=-1', label: 'Graduation Year'}],
    ['studyProgram', {url: 'StudyPrograms?$orderby=id desc&$select=id, name&$top=-1', label: 'Study Program'}],
    ['person/gender', {url: 'Genders?$orderby=id&$select=id, name&$top=-1', label: 'Gender'}],
    ['inscriptionRetroPeriod', {url: 'AcademicPeriods?$orderby=id&$select=id, name&$top=-1', label: 'Inscription Period'}],
    ['inscriptionRetroYear', {url: 'AcademicYears?$orderby=id desc&$select=id, name&$top=-1', label: 'Inscription Year'}]
  ]);

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _translateService: TranslateService) {
    super(router, activatedRoute);
    this.modalClass = 'modal-xl';
    this.modalTitle = 'Rules.Title';
  }

  cancel(): Promise<any> {
    return super.close();
  }

  ngOnInit(): void {
    //
    this.subscription = this.activatedRoute.data.subscribe((routeData) => {
      if (routeData.model == null) {
        throw new Error('Item model cannot be empty at this context.');
      }
      this._context.getMetadata().then((metadata) => {
        const findEntitySet = metadata.EntityContainer.EntitySet.find((item) => {
          return item.Name === routeData.model;
        });
        if (findEntitySet == null) {
          throw new Error('Entity type cannot be found.');
        }
        // set model title from entitySet
        this.modalTitle = this._translateService.instant(`Rules.RuleTypes.${routeData.navigationProperty}`);
        // set target navigation property
        this.itemRulesComponent.navigationProperty = routeData.navigationProperty;
        // set target type
        this.itemRulesComponent.entitySet = findEntitySet.Name;
        // set data
        const data = routeData.data;
        if (Object.prototype.hasOwnProperty.call(routeData, 'department')) {
          Object.assign(data, {'department': routeData.department});
          // apply filter to study program query
          this.dropdownDefinitions.set('studyProgram',
            // tslint:disable-next-line:max-line-length
            {url: this.dropdownDefinitions.get('studyProgram').url + `&$filter=department eq ${routeData.department}`, label: 'Study Program'});
        }
        if (Object.prototype.hasOwnProperty.call(routeData, 'studyProgram')) {
          Object.assign(data, {'studyProgram': routeData.studyProgram && routeData.studyProgram.id});
        }
        // assign dropdown definitions to form data
        Object.assign(data, {'dropdownDefinitions': this.dropdownDefinitions});
        this.itemRulesComponent.target = data;
        this.itemRulesComponent.ngOnChanges({
          target: data
        });
      });
    });
  }


  ok(): Promise<any> {
    return this.itemRulesComponent.save().then(() => {
      return super.close({
          fragment: 'reload',
          skipLocationChange: true
      });
    }).catch((err) => {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

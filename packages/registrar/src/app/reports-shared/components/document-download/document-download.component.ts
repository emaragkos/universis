import {Component, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation, OnChanges} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import {ModalService, LoadingService} from '@universis/common';
import {ResponseError} from '@themost/client';
import {NgxExtendedPdfViewerComponent} from 'ngx-extended-pdf-viewer';
import {DocumentSignComponent} from '@universis/ngx-signer';
import { PDFDocument } from 'pdf-lib';

declare var $: any;

@Component({
  selector: 'app-document-download',
  templateUrl: './document-download.component.html',
  styleUrls: ['../select-report/select-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DocumentDownloadComponent implements OnChanges, OnDestroy {

  @Input() blob: any;
  @Input() showSignButton = true;
  @Input() show = true;
  @Input() onClose: () => void | Promise<void>;
  @Input() documentCode: string;
  @Input() documentName: string;
  @Input() documents: any | any[];

  private subscription: Subscription;
  private changeSubscription: Subscription;
  private queryParamsSubscription: Subscription;
  public recordsTotal: number;
  public currentItem: any;
  public enableSignButton = true;

  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;

  constructor(private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _loadingService: LoadingService
            ) {}

  async ngOnChanges() {
    try {
      this._loadingService.showLoading();
      this.createDocuments();
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   *
   * Prepares the documents before showing them in the pdf viewer
   *
   */
  async createDocuments(): Promise<void> {
    if (this.documents && this.documents.length > 1 && this.show) {
      const mergedPdf = await PDFDocument.create();
      for (const doc of this.documents) {
        const pdfBytes = await doc.blob.arrayBuffer();
        const pdf = await PDFDocument.load(pdfBytes);
        const copiedPages = await mergedPdf.copyPages(pdf, pdf.getPageIndices());
        copiedPages.forEach((page) => {
          mergedPdf.addPage(page);
        });
      }
      const mergedPdfFile = await mergedPdf.save();
      this.blob = mergedPdfFile;
      this.enableSignButton = false;
      this.showViewer();
    } else if (this.documents && this.documents.length === 1 && this.show) {
      this.blob = this.documents[0].blob;
      this.enableSignButton = !this.documents[0].signed;
      this.showViewer();
    } else if (this.documents && !Array.isArray(this.documents)) {
      this.blob = this.documents.blob;
      this.showViewer();
    }
  }

  /**
   * Downloads a file by using the specified document code
   */
  async download(documentCode: any) {
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    this.currentItem = await this._context.model('DocumentNumberSeriesItems')
      .where('documentCode').equal(documentCode)
      .getItem();
    if (this.currentItem == null) {
      throw new ResponseError('Not Found', 404);
    }
    // enable of disable sign button
    this.enableSignButton = !this.currentItem.signed;
    const url = this.currentItem.url;
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const documentURL = this._context.getService().resolve(url.replace(/\\/g, '/').replace(/^\/api\//, ''));
    // get report blob
    return fetch(documentURL, {
      method: 'GET',
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.ok === false) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.queryParamsSubscription) {
      this.queryParamsSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    this.closeViewer();
  }

  showViewer() {
    this.setHeaderZIndex(true);
    document.body.style.overflow = "hidden";
    $(this.pdfViewerContainer.nativeElement).show();
    this.pdfViewer.onResize();
    // add close button
    const toolbarViewerRight = $(this.pdfViewerContainer.nativeElement)
      .find('#toolbarViewerRight');
    if (toolbarViewerRight.find('pdf-close-tool').length === 0) {
      const closeText = this._translateService.instant('Reports.Viewer.Close');
      const closeTool = $(`
        <pdf-close-tool>
            <button style="width:auto" class="toolbarButton px-2" type="button">
                ${closeText}
            </button>
        </pdf-close-tool>
      `);
      // prepare to close viewer
      closeTool.find('button').on('click', () => {
        $(this.pdfViewerContainer.nativeElement).hide();
        document.body.style.overflow = "auto";
      });
      // insert before first which is the pdf hand tool
      closeTool.insertBefore(toolbarViewerRight.find('pdf-hand-tool'));
    }
  }

  closeViewer() {
    $(this.pdfViewerContainer.nativeElement).hide();
    document.body.style.overflow = "auto";
    this.setHeaderZIndex(false);
  }

  pdfClose() {
    this.setHeaderZIndex(false);
    try {
      $(this.pdfViewerContainer.nativeElement).hide();
      document.body.style.overflow = "auto";
      $('.bd-modal').removeClass('modal-pdf-viewer');
      if (this.onClose) {
        this.onClose();
      }
    } catch(err) {
      console.error(err);
    }
  }

  printAction() {
    $(this.pdfViewerContainer.nativeElement).find('pdf-print>button').trigger('click');
  }

  async downloadAction() {
    try {
      this._loadingService.showLoading();
      let documentName = 'document';
      if (this.documents.length === 1 && this.documents[0].documentName) {
        documentName = this.documents[0].documentName;
      } else if (this.documents.length === 1 && this.documents[0].documentCode) {
        const item = await this._context.model('DocumentNumberSeriesItems')
          .where('documentCode').equal(this.documents[0].documentCode)
          .getItem();
        documentName = item.name;
      }

      this.pdfViewer.filenameForDownload = documentName;
      this.pdfViewer.ngOnChanges({
        filenameForDownload: documentName as any
      });

      $(this.pdfViewerContainer.nativeElement).find('pdf-download>button').trigger('click');
    } catch (err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   * Opens an dialog to sign current document
   */
  async sign() {
    if (this.currentItem === null || typeof this.currentItem === 'undefined') {
      const documentCode = this.documents && this.documents.length && this.documents[0].documentCode;
      if (documentCode == null) {
        throw new ResponseError('Not Found', 404);
      }
      this.currentItem = await this._context.model('DocumentNumberSeriesItems')
        .where('documentCode').equal(documentCode)
        .getItem();
      if (this.currentItem === null || typeof this.currentItem === 'undefined') {
        throw new ResponseError('Not Found', 404);
      }
    }
    this._modalService.openModalComponent(DocumentSignComponent, {
      class: 'modal-lg',
      ignoreBackdropClick: true,
      initialState: {
        item: Object.assign({}, this.currentItem, {
          signReport: true
        })
      }
    });
  }

  /**
   * Sign document action
   */
  signAction() {
    return this.sign();
  }

  setHeaderZIndex(high: boolean) {
    const header = document.querySelector('header');
    if (header) {
      header.style.zIndex = high ? '1046' : '1020';
    }
  }
}

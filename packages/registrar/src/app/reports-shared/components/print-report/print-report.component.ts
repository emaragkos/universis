import { Component, OnInit, OnDestroy, Input, ViewChild, TemplateRef, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { UserService, ErrorService, LoadingService } from '@universis/common';
import { base64StringToBlob, blobToBase64String } from 'blob-util';
import { FormioComponent, FormioRefreshValue } from 'angular-formio';
import { TranslateService } from '@ngx-translate/core';
import { BlobContent, ReportService } from '../../services/report.service';
import { Observable, Subscription } from 'rxjs';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { SignerService } from '@universis/ngx-signer';

@Component({
  selector: 'app-print-report',
  templateUrl: './print-report.component.html',
  styles: ['.formio-component .checkbox { position: static !important; }'],
  styleUrls: ['./../select-report/select-report.component.scss'],
})
export class PrintReportComponent implements OnInit, OnDestroy {

  @Input() public selectedReport;
  @Input() public lastError;
  @Input() public reportForm;
  @Input() public variablesForm;
  @Input() public formProperties: any;
  @Input() parentCommands: Observable<string>;

  @Output() refreshSettingsForm: EventEmitter<FormioRefreshValue> = new EventEmitter<FormioRefreshValue>();
  @Output() refreshVariablesForm: EventEmitter<FormioRefreshValue> = new EventEmitter<FormioRefreshValue>();
  @Output() formValidity: EventEmitter<{ isValid: boolean, formName: string }> = new EventEmitter();

  @ViewChild('settingsFormReference') settingsFormReference: FormioComponent;
  @ViewChild('variablesFormReference') variablesFormReference: FormioComponent;
  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;

  public signatures: File[] = [];
  public formData: any = {};
  public blob: any;
  public documents: any[];
  public viewerVisible = false;
  private parentCommandsSubscription: Subscription;
  private signerStatusSubscription: Subscription;
  private settingsFormValidity = undefined;
  private variablesFormValidity = undefined;

  constructor(
    private readonly _userService: UserService,
    private readonly _translateService: TranslateService,
    private readonly _reportsService: ReportService,
    private readonly _errorService: ErrorService,
    private readonly _signerService: SignerService,
    private readonly _loadingService: LoadingService
  ) { }

  ngOnInit() {
    this.tryToGetSignatureGraphic();
    if (this.parentCommands) {
      this.parentCommandsSubscription = this.parentCommands.subscribe((command) => {
        this.handleCommand(command);
      });
    }

    this.signerStatusSubscription = this._signerService.queryStatus().subscribe((serviceStatus) => {
      try {
        this.handleSignerSerivceStatus(serviceStatus);
      } catch (err) {
        console.error(err);
      }
    });
  }

  ngOnDestroy() {
    if (this.parentCommandsSubscription) {
      this.parentCommandsSubscription.unsubscribe();
    }
    if (this.signerStatusSubscription) {
      this.signerStatusSubscription.unsubscribe();
    }
    this._signerService.destroy();
  }

  onFileSelect(event) {
    this._userService.getUser().then((user) => {
      // get file
      const addedFile = event.addedFiles[0];
      // add file
      this.signatures.push(addedFile);
      // save image to local storage
      blobToBase64String(addedFile).then((value) => {
        localStorage.setItem('user.signatureGraphic', value);
        localStorage.setItem('user.signatureGraphic.type', addedFile.type);
        localStorage.setItem('user.signatureGraphic.name', addedFile.name);
      });
    });
  }

  onFileRemove(event) {
    this.signatures.splice(this.signatures.indexOf(event), 1);
    // delete local storage
    localStorage.removeItem('user.signatureGraphic');
    localStorage.removeItem('user.signatureGraphic.type');
    localStorage.removeItem('user.signatureGraphic.name');
  }

  onSettingsFormChange(event) {
    if (event && event.isValid !== undefined) {
      if (event.isValid !== this.settingsFormValidity) {
        this.formValidity.emit({
          formName: 'settings',
          isValid: event.isValid
        });
        this.settingsFormValidity = event.isValid;
      }
    }
  }

  onVariablesFormChange(event) {
    if (event && event.isValid !== undefined) {
      if (event.isValid !== this.variablesFormValidity) {
        this.formValidity.emit({
          formName: 'variables',
          isValid: event.isValid
        });
        this.variablesFormValidity = event.isValid;
      }
    }
  }

  /**
   * 
   * Attempts to populate the user's signatures list with a stored signature image
   * 
   */
  tryToGetSignatureGraphic(): void {
    const value = localStorage.getItem('user.signatureGraphic');
    if (value) {
      const type = localStorage.getItem('user.signatureGraphic.type');
      const name = localStorage.getItem('user.signatureGraphic.name');
      const blob = base64StringToBlob(value, type);
      const addFile = new File([blob], name, { type: type });
      this.signatures.push(addFile);
    }
  }

  /**
   * 
   * Initializes the settings form
   * 
   */
  onSettingsFormLoad(): void {
    try {
      if (this.settingsFormReference == null) {
        return;
      }
      const currentLang = this._translateService.currentLang;
      const translation = this._translateService.instant('Forms');
      if (this.reportForm.settings && this.reportForm.settings.i18n) {
        // try to get local translations
        if (Object.prototype.hasOwnProperty.call(this.reportForm.settings.i18n, currentLang)) {
          // assign translations
          Object.assign(translation, this.reportForm.settings.i18n[currentLang]);
        }
      }
      this.settingsFormReference.formio.i18next.options.resources[currentLang] = { translation: translation };
      this.settingsFormReference.formio.language = currentLang;

      // Initialize the form validity status
      this.onSettingsFormChange({
        isValid: this.settingsFormReference.formio.isValid()
      });
    } catch (err) {
      console.error('on form load error: ', err);
    }
  }

  /**
   * 
   * Calls the report creation
   * 
   */
  async printReport(): Promise<any> {
    let blob;
    this._loadingService.showLoading();
    try {
      // print report by passing report parameters
      // this.selectedReport.id = 15; // TODO: Pass the report variables
      let printData = {} as any;

      // Add report parameters from the parent component
      if (this.formProperties) {
        Object.assign(printData, this.formProperties);
      }

      // Add report parameters from the variables form
      if (this.variablesForm) {
        try {
          Object.assign(printData, this.variablesFormReference.formio.data);
        } catch(err) {
          console.warn("Variables where not set at the report print");
          console.error(err);
        }
      }

      let printSettings = {} as any;
      try {
        printSettings = this.settingsFormReference.formio.data;
        printData.REPORT_USE_DOCUMENT_NUMBER = printSettings.useDocumentNumber;
        printData.REPORT_DOCUMENT_SERIES = printSettings.documentSeries;
      } catch (err) {
        console.error(err);
      }
      // return false;
      blob = await this._reportsService.printReport(this.selectedReport.id, printData);

      // if user had selected to sign document
      if (printSettings.signReport) {
        blob = await this.signAndReplaceDocument(blob, printSettings);
      }
    } catch (err) {
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
    if (blob == null) {
      // exit
      return false;
    }
    // get blob and pass it to pdf viewer
    this.documents = [{
      blob: blob,
      documentName: this.selectedReport.name
    }];
    this.showViewer();
    return false;
  }

  /**
   * 
   * Signs the printed report and replaces it in the server
   * 
   * @param blob The printed report
   * @param formSettings The settings form contents
   * 
   */
  async signAndReplaceDocument(blob, formSettings: any): Promise<any> {
    // get requires verification flag
    const extras = {};
    const requiresVerificationCode = await this._signerService.requiresVerificationCode();
    if (requiresVerificationCode) {
      // ensure loading is hidden
      this._loadingService.hideLoading();
      const verificationCode = await this._signerService.confirmVerificationCode();
      if (verificationCode == null) {
        return;
      }
      Object.assign(extras, {
        otp: verificationCode
      });
    }
    // get content location and prepare to sign
    const contentLocation = (<BlobContent>blob).contentLocation;
    this._loadingService.showLoading();
    try {
      // sign document
      blob = await this._signerService.signDocument(blob, formSettings.signCertificate, null, this.signatures[0], extras);
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      this._loadingService.hideLoading();
    }
    // replace document
    if (contentLocation) {
      // replace document (add signed)
      await this._signerService.replaceDocument({
        url: contentLocation,
        signed: true,
        published: false
      }, blob);
      // and continue
    }

    return blob;
  }

  /**
   * 
   * Shows the pdf viewer
   * 
   */
  showViewer() {
    this.viewerVisible = true;
  }

  /**
   * 
   * Handles the commands from outside components
   * 
   * @param command The command to be executed by this component
   * 
   */
  async handleCommand(command: string) {
    if (command === 'submit') {
      this.printReport();
    }
  }

  // Pdf viewer buttons
  pdfPrint() {
    $(this.pdfViewerContainer.nativeElement).find('pdf-print>button').trigger('click');
  }

  pdfDownload() {
    const reportName = <any>this.selectedReport.name;
    this.pdfViewer.filenameForDownload = reportName;
    this.pdfViewer.ngOnChanges({
      filenameForDownload: reportName
    });
    $(this.pdfViewerContainer.nativeElement).find('pdf-download>button').trigger('click');
  }

  closeViewer() {
    $(this.pdfViewerContainer.nativeElement).hide();
    $('.bd-modal').removeClass('modal-pdf-viewer');
  }

  /**
   * A helper function for finding a component by key name
   */
  findComponent(components: Array<any>, key: any) {
    for (let index = 0; index < components.length; index++) {
      const component = components[index];
      if (component.key === key) {
        return component;
      }
      if (component.columns) {
        for (let j = 0; j < component.columns.length; j++) {
          const column = component.columns[j];
          if (Array.isArray(column.components)) {
            const findComponent = this.findComponent(column.components, key);
            if (findComponent) {
              return findComponent;
            }
          }
        }
      }
      if (component.components) {
        const findComponent = this.findComponent(component.components, key);
        if (findComponent) {
          return findComponent;
        }
      }
    }
  }

  /**
   * Get certificate list from signer app
   */
  onCustomEvent(event: any) {
    if (event.type === 'certificates') {
      this.lastError = null;
      // get data
      const data = this.settingsFormReference.submission.data;
      // get form
      const form = this.settingsFormReference.form;
      const authenticate = new Promise<void>((resolve, reject) => {
        if (this._signerService.authorization != null) {
          return resolve();
        }
        this._signerService.requiresUsernamePassword().then((requiresUsernamePassword: any) => {
          return this._signerService.authenticate({
            username: requiresUsernamePassword ? data.typeKeyStoreUsername : 'user',
            password: data.typeKeyStorePassword,
            rememberMe: data.keepMeSignedInDuringThisSession
          }).then(() => {
            return resolve();
          });
        }).catch((err) => {
          return reject(err);
        });
      });
      authenticate.then(() => {
        this._signerService.getCertificates().then((certs: Array<any>) => {
          // find component by key
          const selectComponent = this.findComponent(form.components, 'signCertificate');
          if (selectComponent) {
            // map certificate collection
            selectComponent.data.values = certs
              .filter(cert => cert.expired === false)
              .map((cert) => {
              return {
                label: cert.commonName,
                value: cert.thumbprint
              };
            });
            if (selectComponent.data.values.length) {
              data.signCertificate = selectComponent.data.values[0].value;
            }
            // refresh form and data
            this.refreshSettingsForm.emit({
              form: form,
              submission: {
                data: data
              }
            });
          }
        }).catch((err) => {
          this.lastError = err;
        });
      }).catch((err) => {
        this.lastError = err;
      });
    }
  }

  async handleSignerSerivceStatus(serviceStatus) {
    if (serviceStatus != null) {
      const formData = Object.assign({}, this.selectedReport, {
        signerServiceStatus: serviceStatus.status
      });
      // if signer service is already authenticated
      if (this._signerService.authorization) {
        // get certificates
        return this._signerService.getCertificates().then((certs) => {
          // find select certificate component
          const selectComponent = this.findComponent(this.reportForm.components, 'signCertificate');
          // and add certificate values
          if (selectComponent) {
            // map certificate collection
            selectComponent.data.values = certs
              .filter(cert => cert.expired === false)
              .map((cert) => {
              return {
                label: cert.commonName,
                value: cert.thumbprint
              };
            });
          }
          const lastCertificate = this._signerService.getLastCertificate();
          if (lastCertificate) {
            // find if last certificate exists
            const findCertificate = certs.find(cert => {
              return cert.thumbprint = lastCertificate;
            });
            if (findCertificate) {
              Object.assign(formData, {
                signCertificate: findCertificate.thumbprint
              });
            }
          } else if (certs.length) {
            Object.assign(formData, {
              signCertificate: certs[0].thumbprint
            });
          }
          // refresh report form data
          Object.assign(formData, {
            signerAlreadyAuthenticated: true,
            keepMeSignedInDuringThisSession: true,
            typeKeyStorePassword: '****'
          });
          // refresh form and data
          this.refreshSettingsForm.emit({
            form: this.reportForm,
            submission: {
              data: formData
            }
          });
        }).catch((err) => {
          this.lastError = err;
        });
      } else {
        // refresh report form data
        Object.assign(formData, {
          signerAlreadyAuthenticated: false,
          keepMeSignedInDuringThisSession: false,
          typeKeyStorePassword: null
        });
      }
      // refresh form and data
      this.refreshSettingsForm.emit({
        form: this.reportForm,
        submission: {
          data: formData
        }
      });
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScholarshipsTableComponent } from './scholarships-table.component';

describe('ScholarshipsTableComponent', () => {
  let component: ScholarshipsTableComponent;
  let fixture: ComponentFixture<ScholarshipsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScholarshipsTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScholarshipsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

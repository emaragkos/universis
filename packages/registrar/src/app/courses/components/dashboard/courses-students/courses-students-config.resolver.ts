import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {TableConfiguration} from '@universis/ngx-tables';

export class CoursesStudentsTableConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
      return import(`./courses-students.config.${route.params.list}.json`)
      .catch( err => {
          return  import(`./courses-students.config.list.json`);
      });
  }
}

export class CoursesStudentsTableSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
      return import(`./courses-students.search.${route.params.list}.json`)
      .catch( err => {
          return  import(`./courses-students.search.list.json`);
      });
  }
}

export class CoursesStudentsDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
      return import(`./courses-students.config.list.json`);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesOverviewStudyProgrammsComponent } from './courses-overview-study-programms.component';

describe('CoursesOverviewStudyProgrammsComponent', () => {
  let component: CoursesOverviewStudyProgrammsComponent;
  let fixture: ComponentFixture<CoursesOverviewStudyProgrammsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesOverviewStudyProgrammsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesOverviewStudyProgrammsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

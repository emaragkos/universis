import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import * as GRADUATIONS_LIST_CONFIG from './graduations-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedTableService} from '@universis/ngx-tables';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';

@Component({
  selector: 'app-graduations-table',
  templateUrl: './graduations-table.component.html'
})
export class GraduationsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  public showPublish = true;
  public showSign = true;
  public recordsTotal: any;

  public readonly config = GRADUATIONS_LIST_CONFIG;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService) { }

  ngOnInit() {
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.table.fetch(true);
      }
    });
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }

      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Graduations.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}


import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-graduations-dashboard',
  templateUrl: './graduations-dashboard.component.html'
})
export class GraduationsDashboardComponent implements OnInit {

  public tabs: any[];

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );
  }

}

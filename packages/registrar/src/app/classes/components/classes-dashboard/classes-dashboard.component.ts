import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-classes-dashboard',
  templateUrl: './classes-dashboard.component.html',
  styleUrls: ['./classes-dashboard.component.scss']
})
export class ClassesDashboardComponent implements OnInit {

  public model: any;
  public tabs: any[];

  constructor(private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {

    this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

  }

}

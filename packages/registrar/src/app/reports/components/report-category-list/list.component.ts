import { Component, OnInit, ViewChild, OnDestroy, Input } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent,
  AdvancedTableDataResult,
  AdvancedTableConfiguration } from '@universis/ngx-tables';
import { ErrorService, DIALOG_BUTTONS, ModalService, UserActivityService } from '@universis/common';
import {ResponseError} from '@themost/client';
import { TableConfiguration } from '@universis/ngx-tables';
import { SettingsService, SettingsSection } from '../../../settings-shared/services/settings.service';
import * as LIST_CONFIG from './report-category.config.list.json';
import { ToastService } from '@universis/common';

declare interface EdmEntityTypeMapping {
  Name: string;
  Description: string;
  LongDescription?: string;
}

@Component({
  selector: 'app-report-category-list',
  templateUrl: './list.component.html'
})
export class ReportCategoryListComponent implements OnInit, OnDestroy {

  public model: Array<EdmEntityTypeMapping>;
  public recordsTotal: number;
  public config: TableConfiguration;
  private subscription: Subscription;
  private sectionSubscription: Subscription;
  private reloadSubscription: Subscription;
  public section: SettingsSection;
  @Input('entitySet') entitySet = 'ReportCategories';
  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _activatedRoute: ActivatedRoute,
    private _errorService: ErrorService,
    private _settings: SettingsService,
    private _toastService: ToastService,
    private _modalService: ModalService,
    private _userActivityService: UserActivityService
    ) { }
  ngOnDestroy(): void {
    if (this.sectionSubscription) {
      this.sectionSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ngOnInit() {
      try {
        const metadata = await this._context.getMetadata();
        // get entityType
        const re = new RegExp(`^${this.entitySet}$`, 'ig');
        const entitySet = metadata.EntityContainer.EntitySet.find( x => {
          return re.test(x.Name);
        });
        // if entity type is null
        if (entitySet == null) {
          // navigate to error
          return this._errorService.navigateToError(new ResponseError('EntitySet Not Found', 404));
        }
        // get entity type
        const entityType = metadata.EntityType.find( x => {
          return x.Name === entitySet.EntityType;
        });
        if (entityType == null) {
          return this._errorService.navigateToError(new ResponseError('EntityType Not Found', 404));
        }
        // if entity type does not implement Enumeration
        if (entityType.ImplementsType !== 'Enumeration') {
          // navigate to error
          return this._errorService.navigateToError(new ResponseError('Invalid EntityType', 500));
        }
        // get sections as promise
        this.sectionSubscription = this._settings.sections.subscribe( sections => {
          this.section = sections.find( x => {
            return x.name === entityType.Name;
          });
          // clone default configuration
          const defaultConfiguration = AdvancedTableConfiguration.cast(LIST_CONFIG, true);
          // correct edit button link
          const column = defaultConfiguration.columns.find( col => {
            return col.formatter === 'ButtonFormatter' && col.formatString === '(modal:edit)';
          });
          column.formatString = '#/reports/configuration/categories/(modal:${id}/edit)';
          // create list configuration by assigning list entity set
          const tableConfiguration = Object.assign({
            model: entitySet.Name
          }, defaultConfiguration);
          // set configuration to table
          this.table.config = tableConfiguration;
          // fetch data
          this.table.fetch(true);
          // do reload by using hidden fragment e.g. /classes#reload
          this.reloadSubscription = this._activatedRoute.fragment.subscribe(fragment => {
            if (fragment && fragment === 'reload') {
              this._toastService.show(
                this.section.description,
                this._translateService.instant('Settings.OperationCompleted')
              );
              this.table.fetch(true);
            }
          });
          // finally add user activity
        this._userActivityService.setItem({
          category: this._translateService.instant('Reports.Title'),
          description: this.section.description,
          url: window.location.hash.substring(1),
          dateCreated: new Date
        });
        });
      } catch (err) {
        console.error(err);
        return this._errorService.showError(err, {
          continueLink: '/settings'
        });
      }
  }

  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  /**
   * Remove selected items
   */
  remove() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Tables.RemoveItemsTitle'),
        this._translateService.instant('Tables.RemoveItemsMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model(this.table.config.model).remove(items).then(() => {
              this._toastService.show(
                this.section.description + ':' + this._translateService.instant('Tables.RemoveItemMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Tables.RemoveItemMessage.one' : 'Tables.RemoveItemMessage.many')
                  , { value: items.length })
              );
              this.table.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}


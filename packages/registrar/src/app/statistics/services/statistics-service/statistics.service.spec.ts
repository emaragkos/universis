import { TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { StatisticsRoutingModule } from './../../statistics.routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import { ReportsSharedModule } from '../../../reports-shared/reports-shared.module';
import { StatisticsService } from './../../services/statistics-service/statistics.service';
import { SectionsComponent } from './../../components/sections/sections.component';
import { HomeComponent } from './../../components/home/home.component';
import { SettingsModule } from './../../../settings/settings.module';
import { MostModule } from '@themost/angular';
import { SettingsService } from './../../../settings-shared/services/settings.service';
import { AuthModule, ConfigurationService } from '@universis/common';
import { TestingConfigurationService } from '@universis/common/testing';

describe('StatisticsService', () => {
  beforeEach(() => {
    return TestBed.configureTestingModule({
      imports: [
        CommonModule,
        AuthModule,
        TranslateModule.forRoot(),
        StatisticsRoutingModule,
        FormsModule,
        SettingsModule,
        MostModule.forRoot({
          base: '/',
          options: {
            useMediaTypeExtensions: false
          }
        }),
        NgArrayPipesModule,
        ReportsSharedModule.forRoot()
      ],
      declarations: [HomeComponent, SectionsComponent],
      providers: [
        SettingsService,
        StatisticsService,
        {
          provide: ConfigurationService,
          useClass: TestingConfigurationService
        }
      ]
    }).compileComponents();
  });

  it('should be created', () => {
    const service: StatisticsService = TestBed.get(StatisticsService);
    expect(service).toBeTruthy();
  });
});

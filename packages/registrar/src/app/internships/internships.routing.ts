import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InternshipsHomeComponent } from './components/internships-home/internships-home.component';
import { InternshipsTableComponent } from './components/internships-table/internships-table.component';
import { InternshipsPreviewComponent } from './components/internships-preview/internships-preview.component';
import { InternshipsRootComponent } from './components/internships-root/internships-root.component';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';
import { AdvancedFormRouterComponent } from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver, ActiveDepartmentIDResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {
  InternshipsTableConfigurationResolver,
  InternshipsTableSearchResolver
} from './components/internships-table/internships-table-config.resolver';
import { AdvancedListComponent } from '@universis/ngx-tables';
import { AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver } from '@universis/forms';

import * as TableCompaniesConfiguration from './components/internships-table/internships-table.config.companies.json';
import { InternshipCandidatesComponent } from './components/internship-candidates/internship-candidates.component';

const routes: Routes = [
  {
    path: 'configuration/companies',
    component: AdvancedListComponent,
    data: {
      model: 'Companies',
      tableConfiguration: TableCompaniesConfiguration,
      category: 'Internships.Title',
      description: 'Settings.Lists.Company.Description',
      longDescription: 'Settings.Lists.Company.LongDescription'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true,
          description: null
        },
        resolve: {
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          serviceQueryParams: {
          },
          closeOnSubmit: true,
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: '',
    component: InternshipsHomeComponent,
    data: {
      title: 'Internships'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: InternshipsTableComponent,
        data: {
          title: 'Internships'
        },
        resolve: {
          tableConfiguration: InternshipsTableConfigurationResolver,
          searchConfiguration: InternshipsTableSearchResolver
        },
        children: [
          {
            path: 'add',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Internships',
              description: null,
              action: 'new',
              closeOnSubmit: true
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              department: ActiveDepartmentIDResolver,
              internshipYear: CurrentAcademicYearResolver,
              internshipPeriod: CurrentAcademicPeriodResolver
            }
          },
          {
            path: ':id/edit',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData>{
              model: 'Internships',
              action: 'edit',
              closeOnSubmit: true,
              serviceQueryParams: {
                $expand: 'department,status,student($expand=person),company',
                $levels: 3
              }
            },
            resolve: {
              formConfig: AdvancedFormResolver,
              data: AdvancedFormItemResolver
            }
          }
        ]
      }
    ]
  },
  {
    path: ':id',
    component: InternshipsRootComponent,
    data: {
      title: 'Internship Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'preview'
      },
      {
        path: 'preview',
        component: InternshipsPreviewComponent,
        data: {
          title: 'Internship preview'
        },
        children: [
          {
            path: '',
            redirectTo: 'general'
          },
          {
            path: 'general',
            component: InternshipsPreviewGeneralComponent,
            data: {
              title: 'Internship general information'
            },
            children: [
              {
                path: 'edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'Internships',
                  action: 'edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'department,status,student($expand=person),company',
                    $levels: 3
                  }
                },
                resolve: {
                  formConfig: AdvancedFormResolver,
                  data: AdvancedFormItemResolver
                }
              }
            ]
          },
          {
            path: 'candidates',
            component: InternshipCandidatesComponent,
            data: {
              title: 'Internship candidates'
            },
            children: [
            ]
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class InternshipsRoutingModule {
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import { StudentsTableComponent } from './components/students-table/students-table.component';
import { StudentsRootComponent } from './components/students-root/students-root.component';
import { StudentsGeneralComponent } from './components/students-dashboard/students-general/students-general.component';
import { StudentsGradesComponent } from './components/students-dashboard/students-grades/students-grades.component';
import { StudentsThesesComponent } from './components/students-dashboard/students-theses/students-theses.component';
import { StudentsOverviewComponent } from './components/students-dashboard/students-overview/students-overview.component';
import { StudentsCoursesComponent } from './components/students-dashboard/students-courses/students-courses.component';
import { StudentsRegistrationsComponent } from './components/students-dashboard/students-registrations/students-registrations.component';
import { StudentsScholarshipsComponent } from './components/students-dashboard/students-scholarships/students-scholarships.component';
import { StudentsRequestsComponent } from './components/students-dashboard/students-requests/students-requests.component';
import { StudentsInternshipsComponent } from './components/students-dashboard/students-internships/students-internships.component';
import { StudentsMessagesComponent } from './components/students-dashboard/students-messages/students-messages.component';
import {
  AdvancedFormItemWithLocalesResolver,
  AdvancedFormRouterComponent,
  AdvancedFormStudentWithLocalesResolver
} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { StudentsDashboardComponent } from './components/students-dashboard/students-dashboard.component';
import {
  ActiveDepartmentIDResolver,
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver, LastStudyProgramResolver
} from '../registrar-shared/services/activeDepartmentService.service';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData,
  AdvancedFormParentItemResolver, AdvancedFormResolver
} from '@universis/forms';
import {StudentTableConfigurationResolver, StudentTableSearchResolver} from './components/students-table/student-table-config.resolver';
import {StudentsSharedModule} from './students.shared';
import { SelectReportComponent } from '../reports-shared/components/select-report/select-report.component';
import { ItemDocumentsComponent } from '../reports-shared/components/item-documents/item-documents.component';
import { StudentsRequestsSearchResolver } from './components/students-dashboard/students-requests/students-requests-config.resolver';
import {StudentsRulesComponent} from './components/students-dashboard/students-rules/students-rules.component';
import {StudentsInformationsComponent} from './components/students-dashboard/students-information/students-informations.component';
import {ItemRulesModalComponent, RuleFormModalData} from '../rules';
import { StudentsAttachmentsComponent } from './components/students-dashboard/students-attachments/students-attachments.component';
import { StudentProgramGroupsResolver } from './student-program-groups-resolver';

const routes: Routes = [
  {
    path: '',
    component: StudentsHomeComponent,
    data: {
      title: 'Students'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/active'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: StudentsTableComponent,
        data: {
          title: 'Students'
        },
        resolve: {
          department: ActiveDepartmentResolver,
          tableConfiguration: StudentTableConfigurationResolver,
          searchConfiguration: StudentTableSearchResolver
        },
        children: [
          {
            path: ':id/complete-inscription',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'Students',
              action: 'inscription-edit'
            },
            resolve: {
              data: AdvancedFormItemResolver
            }
          }
        ]
      }
    ]
  },
  {
    path: 'create',
    component: StudentsRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        data: {
          inscriptionDate: new Date(),
          inscriptionSemester: 1,
        }
      }
    ],
    resolve: {
      department: ActiveDepartmentResolver,
      inscriptionYear: CurrentAcademicYearResolver,
      inscriptionPeriod: CurrentAcademicPeriodResolver,
      studyProgram: LastStudyProgramResolver
    }
  },
  {
    path: ':id',
    component: StudentsRootComponent,
    data: {
      title: 'Student Home'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: StudentsDashboardComponent,
        data: {
          title: 'Student Dashboard'
        },
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'overview'
          },
          {
            path: 'overview',
            component: StudentsOverviewComponent,
            data: {
              title: 'Students.Overview'
            },
            children: [
              {
                path: 'print',
                pathMatch: 'full',
                component: SelectReportComponent,
                outlet: 'modal',
                resolve: {
                  item: AdvancedFormItemResolver
                }
              },
              {
                path: 'inscription-edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'Students',
                  action: 'inscription-edit',
                  closeOnSubmit: true
                },
                resolve: {
                  student: AdvancedFormParentItemResolver,
                  data: AdvancedFormItemResolver,
                  department: ActiveDepartmentIDResolver,
                }
              },
              {
                path: 'internship/new',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'Internships',
                  action: 'new',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'department',
                  }
                },
                resolve: {
                  student: AdvancedFormParentItemResolver,
                  data: AdvancedFormItemResolver,
                  internshipYear: CurrentAcademicYearResolver,
                  internshipPeriod: CurrentAcademicPeriodResolver,
                  department: ActiveDepartmentResolver,
                }
              },
              {
                path: 'new-request-document',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'RequestDocumentActions',
                  closeOnSubmit: true,
                  continueLink: '/requests/${id}/edit',
                  action: 'new',
                  serviceQueryParams: {
                    $expand: 'department',
                  }
                },
                resolve: {
                  student: AdvancedFormParentItemResolver
                }
              },
              {
                path: 'remove-request',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'RequestRemoveActions',
                  action: 'new',
                  closeOnSubmit: true,
                  continueLink: '/requests/${id}/edit',
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  student: AdvancedFormParentItemResolver,
                  removalYear: CurrentAcademicYearResolver,
                  removalPeriod: CurrentAcademicPeriodResolver
                }
              },
              {
                path: 'suspend-request',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'RequestSuspendActions',
                  action: 'new',
                  continueLink: '/requests/${id}/edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  student: AdvancedFormParentItemResolver,
                  suspensionYear: CurrentAcademicYearResolver,
                  suspensionPeriod: CurrentAcademicPeriodResolver
                }
              },
              {
                path: 'suspensions/:id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'StudentSuspensions',
                  action: 'edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'student($expand=studentStatus,person)',
                  }
                },
                resolve: {
                  data: AdvancedFormItemResolver
                }
              },
              {
                path: 'counselors/:id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'StudentCounselors',
                  action: 'edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'instructor,toYear,toPeriod,student($expand=person)',
                    $levels: 3
                  }
                },
                resolve: {
                  formConfig: AdvancedFormResolver,
                  data: AdvancedFormItemResolver
                }
              },
              {
                path: 'add-program-group',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData>{
                  model: 'UpdateStudentGroupActions',
                  action: 'addStudentGroup',
                  closeOnSubmit: true
                },
                resolve: {
                  department: ActiveDepartmentIDResolver,
                  formConfig: AdvancedFormResolver,
                  data: StudentProgramGroupsResolver
                }
              }
            ]
          },
          {
            path: 'general',
            component: StudentsGeneralComponent,
            data: {
              title: 'Students.General'
            }
          },
          {
            path: 'courses',
            component: StudentsCoursesComponent,
            data: {
              title: 'Students.Courses'
            }
          },
          {
            path: 'theses',
            component: StudentsThesesComponent,
            data: {
              title: 'Students.Theses'
            }
          },
          {
            path: 'registrations',
            component: StudentsRegistrationsComponent,
            data: {
              title: 'Students.Registrations'
            }
          },
          {
            path: 'grades',
            component: StudentsGradesComponent,
            data: {
              title: 'Students.Grades'
            }
          },
          {
            path: 'documents',
            component: ItemDocumentsComponent,
            data: {
              title: 'Documents.ItemDocuments'
            }
          },
          {
            path: 'requests',
            component: StudentsRequestsComponent,
            data: {
              title: 'Students.Requests'
            },
            resolve: {
              searchConfiguration: StudentsRequestsSearchResolver
            }
          },
          {
            path: 'scholarships',
            component: StudentsScholarshipsComponent,
            data: {
              title: 'Students.Scholarships'
            }
          },
          {
            path: 'internships',
            component: StudentsInternshipsComponent,
            data: {
              title: 'Students.Internships'
            }
          },
          {
            path: 'messages',
            component: StudentsMessagesComponent,
            data: {
              title: 'Students.Messages'
            }
          },
          {
            path: 'rules',
            component: StudentsRulesComponent,
            data: {
              title: 'Students.Rules'
            },
            children: [
              {
                path: 'graduateRules',
                pathMatch: 'full',
                component: ItemRulesModalComponent,
                outlet: 'modal',
                data: <RuleFormModalData> {
                  model: 'Students',
                  closeOnSubmit: true,
                  navigationProperty: 'StudentGraduationRules',
                },
                resolve: {
                  data: AdvancedFormItemResolver,
                  department: ActiveDepartmentIDResolver
                }
              },
            ]
          },
          {
            path: 'informations',
            component: StudentsInformationsComponent,
            data: {
              title: 'Students.AdditionalInfo'
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'StudentInformations',
                  closeOnSubmit: true,
                  action: 'new',
                  infoDate: new Date(),
                },
                resolve: {
                  student: AdvancedFormParentItemResolver
                }
              },
              {
                path: ':id/edit',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'StudentInformations',
                  action: 'edit',
                  closeOnSubmit: true,
                  serviceQueryParams: {
                    $expand: 'infoType,student($expand=person)',
                  }
                },
                resolve: {
                  data: AdvancedFormItemResolver,
                }
              }
            ]
          },
          {
            path: 'attachments',
            component: StudentsAttachmentsComponent,
            data: {
              title: 'Students.Attachments.Attachments'
            }
          }
        ]
      },
      {
        path: 'remove',
        component: AdvancedFormRouterComponent,
        data: {
          model: 'StudentRemoveActions',
          action: 'new',
          serviceParams : {

          },
          id: null,
          actionStatus: 3
        },
        resolve: {
          object: AdvancedFormParentItemResolver,
          removalYear: CurrentAcademicYearResolver,
          removalPeriod: CurrentAcademicPeriodResolver,
        }
      },
      {
        path: 'edit',
        component: AdvancedFormRouterComponent,
        data: {
          action: 'edit',
          serviceQueryParams: {
            $expand: 'studyProgram, person($expand=locales), department'
          }
        },
        resolve: {
          data: AdvancedFormStudentWithLocalesResolver
        }
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    StudentsSharedModule
  ],
  exports: [RouterModule],
  declarations: []
})
export class StudentsRoutingModule {
}

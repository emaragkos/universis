import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class StudentsInformationsConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./students-informations.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`packages/registrar/src/app/students/components/students-dashboard/students-information/students-informations.config.list.json`);
      });
  }
}



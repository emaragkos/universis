import {Component, ElementRef, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import * as STUDENTS_REGISTRATIONS_LIST_CONFIG from './students-registrations.conflig.list.json';


@Component({
  selector: 'app-students-registrations',
  templateUrl: './students-registrations.component.html'
})
export class StudentsRegistrationsComponent implements OnInit, OnDestroy  {
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>STUDENTS_REGISTRATIONS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('model') model: AdvancedTableComponent;
  @Input() tableConfiguration: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this._activatedTable.activeTable = this.model;
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model.query = this._context.model('Students/' + params.id + '/registrations')
        .asQueryable()
        .orderByDescending('registrationYear')
        .thenByDescending('registrationPeriod')
        .prepare();
      this._loadingService.hideLoading();
      this.model.config = AdvancedTableConfiguration.cast(STUDENTS_REGISTRATIONS_LIST_CONFIG);
      this.model.fetch();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}

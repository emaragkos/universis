import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import * as SEARCH_CONFIG from './student-courses-table.search.list.json';
import {ConfigurationService, ErrorService, GradeScale, LoadingService, ModalService, TemplatePipe} from '@universis/common';
import {StudentsCoursesExemptionComponent} from '../students-courses-exemption/students-courses-exemption.component';
import {GradePipe} from '@universis/common';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';
import {EditCoursesComponent} from '../../../../../study-programs/components/preview/edit-courses/edit-courses.component';
import { AdvancedAggregatorConfig } from '@universis/ngx-tables';
import {GradesService} from './../../../../services/grades-service/grades-service';
import * as STUDENTS_COURSES_GROUP from './students-courses-by-group.config.json';

@Component({
  selector: 'app-students-courses-by-group',
  templateUrl: './students-courses-by-group.component.html',
  styleUrls: ['./students-courses-by-group.component.scss']
})
export class StudentsCoursesByGroupComponent implements OnInit , OnDestroy {
  public model: any;
  public data: any;
  public groups = [];
  public studentId;
  private subscription: Subscription;
  public student: any;
  public advancedAggregatorConfiguration = STUDENTS_COURSES_GROUP as any as AdvancedAggregatorConfig;
  private selectedGroups = new BehaviorSubject({});
  public filters = new BehaviorSubject({filters: [], text: undefined});
  public selectedGroups$: Observable<any>;
  public filters$: Observable<any>;

  @ViewChild('search') search: AdvancedSearchFormComponent;
  collapsed = true;

  public searchExpression = '(student eq ${student} and (indexof(course/name, \'${text}\') ge 0 or indexof(course/displayCode, \'${text}\') ge 0 or ' +
    'indexof(course/courseArea/name, \'${text}\') ge 0 or indexof(course/courseCategory/name, \'${text}\') ge 0 or indexof(courseType/name, \'${text}\') ge 0))';

  public allStudentCourses: any;

  public defaultGradeScale: any;

  public gradeScale: GradeScale;
  public aggregatorList = {
    'total-grade-included-courses': (items: any[]) => {
      return items
        .filter((item) => item.isPassed === 1 || item.registrationType === 1)
        .filter((item) => item.calculateUnits === 1)
        .length;
    },
    'simple-grade-average': (items) => { return this.calculateSimpleGradeAverage(items); } ,
    'formatted-grade-average': (items) => { return this.calculateFormattedGradeAverage(items); },
    'passed-ects': (items: any[]) => {
      return items
        .filter((item) => item.isPassed === 1)
        .filter((item) => item.calculateUnits === 1)
        .reduce((total, item) => total + item.ects, 0)
        .toFixed(2);
    }
  }

  public postProcessors;

  public group1: any;
  public group2: any;
  public aggregatorListData = {};

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _router: Router,
              private _errorService: ErrorService,
              private _advancedFilter: AdvancedFilterValueProvider,
              private _gradePipe: GradePipe,
              private _configService: ConfigurationService,
              private _gradesService: GradesService) {}

  async  ngOnInit() {
    this.filters = new BehaviorSubject({filters: [], text: undefined});
    const defaultGroup = this.advancedAggregatorConfiguration.groupProperties.find(
      (item) => item.default
    ) || this.advancedAggregatorConfiguration.groupProperties[0];
    this.group1 = defaultGroup;
    this.selectedGroups = new BehaviorSubject([defaultGroup]);
    this.selectedGroups$ = this.selectedGroups.asObservable();
    this.filters$ = this.filters.asObservable();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      try {
        this._loadingService.showLoading();
        this.student = await this._context.model('Students')
          .where('id')
          .equal(params.id)
          .expand('studyProgram($expand=gradeScale)')
          .getItem();

        this.aggregatorListData = {
          student: this.student.id
        }

        this.defaultGradeScale = this.student && this.student.studyProgram && this.student.studyProgram.gradeScale;

        if(this.defaultGradeScale.scaleType !== 0){
          this.defaultGradeScale = await this._context.model('GradeScales')
            .where('id').equal(this.defaultGradeScale.id)
            .expand('values')
            .getItem();
        }

        this.gradeScale = new GradeScale(this._configService.currentLocale, this.defaultGradeScale);
        this.model = await this._context.model('StudentCourses')
          .where('student').equal(params.id)
          .expand('programGroup, course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor)), studyProgramSpecialty')
          .take(-1)
          .getItems();

        this.allStudentCourses = this.model;
        this.postProcessors = {
          'combine-complex': (items) => {
            return this.parseCourseParts(this.allStudentCourses, items);
          },
          'combine-exam-period-data': (items) => {
            return ((items)=> {
              items.forEach(x => {
                if(x.gradeExam && x.gradeExam.examPeriod && x.gradeExam.examPeriod.alternateName && x.gradeExam.year && x.gradeExam.year.alternateName){
                  x.gradeExam.year.alternateName = x.gradeExam.examPeriod.alternateName + " " + x.gradeExam.year.alternateName
                }
              });
              return items;
            })(items);
          }
        }
        this._loadingService.hideLoading();
        this.search.form = SEARCH_CONFIG;
        Object.assign(this.search.form, { student: params.id });
        this.search.ngOnInit();
      } catch (err) {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    }, (err) => {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  parseCourseParts(allItems: any[], items: any[]) {
    // get parent courses for orphaned children after search
    const parents = allItems.filter(el => {
        return !items.map(x => x.course.id).includes(el.course.id);
    }).filter(el => {
        return items.map(x => x.parentCourse).includes(el.course.id);
    });

    let data = [];
    try {
      // remove course parts and add parents
      data = items.filter(studentCourse => {
        return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
      }).concat(parents);
    } catch(err) {
      console.error('err: ', err);
    }

    // add courseParts as courses to each parent course
    data.forEach( studentCourse => {
      // get course parts
      if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
        studentCourse.subitems = items.filter(course => {
          return course.parentCourse === studentCourse.course.id;
        }).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });
      }
    });

    return data;
  }

  async onSearchKeyDown(event: any) {
    const searchText = (<HTMLInputElement>event.target);
    if (searchText && event.keyCode === 13) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        this.filters.next({
          filters: [],
          text: searchText.value
        });
        // this.parseCourseParts();
      });
    }
  }

  async advancedSearch(event?: any) {
    if (Array.isArray(this.search.form.criteria)) {
      const expressions = [];
      const filter = this.search.filter;
      const values = this.search.formComponent.formio.data;
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;

        this.search.form.criteria.forEach((x) => {

          if (Object.prototype.hasOwnProperty.call(filter, x.name)) {
            // const nameFilter = this.convertToString(filter[x.name]);
            if (filter[x.name] !== 'undefined') {
              expressions.push(this._template.transform(x.filter, Object.assign({
                value: filter[x.name]
              }, values)));
            }
          }
        });
        // expressions.push('(student eq ' +  this.studentId  + ')');

        this.filters.next({filters: expressions, text: undefined});
        // this.parseCourseParts();
      });
    }
  }

  async editCourse(entry: any) {

    let course = await this._context.model('StudentCourses')
      .where('student').equal(this.student.id)
      .where('id').equal(entry.id)
      .expand('programGroup, course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor)), studyProgramSpecialty')
      .take(1)
      .getItem();

    this._loadingService.showLoading();
    this._modalService.openModalComponent(EditCoursesComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: false,
      initialState: {
        items: [course],
        modalTitle: 'Students.EditCourse',
        formEditName: 'StudentCourses/course-attributes/edit',
        toastHeader: 'Students.EditStudentCourse',
        courseProperties: {
          courseTitle: course.courseTitle,
          notes: course.notes,
          calculateGrade:  course.calculateGrade !== 0,
          calculateUnits: course.calculateUnits !== 0,
          specialty: course.studyProgramSpecialty,
          programGroup: course.programGroup ? course.programGroup : {},
          student: this.student,
          registrationType: course.registrationType !== 0,
          formattedGradeNum: !course.course.gradeScale.scaleType ? course.formattedGrade : null,
          formattedGradeVal: course.course.gradeScale.scaleType ? course.formattedGrade: null,
          gradeScale: course.course.gradeScale,
          gradeYear: course.gradeYear,
          gradePeriod: course.gradePeriod
        },
        execute:  (() => {
          return new Observable((observer) => {
            this._loadingService.showLoading();
            // get add courses component
            const component = <EditCoursesComponent>this._modalService.modalRef.content;
            // fix data structure
            const submissionData = component.items.map(async(x) => {
              if(x.gradeScale.scaleType){
                x.formattedGrade = x.formattedGradeVal;
              }else {
                x.formattedGrade = x.formattedGradeNum
              }

              let currentGradeScale;
              if(x.course && x.course.gradeScale && x.course.gradeScale.id && x.course.gradeScale.id !== this.defaultGradeScale.id ){
                const item = await this._context.model('GradeScales')
                  .where('id').equal(x.course.gradeScale.id)
                  .expand('values')
                  .getItem();

                currentGradeScale = new GradeScale(this._configService.currentLocale, item);
              } else {
                currentGradeScale = this.gradeScale;
              }

              return {
                calculateGrade: x.calculateGrade ? 1 : 0,
                calculateUnits: x.calculateUnits ? 1 : 0,
                coefficient: x.coefficient,
                course: x.course.id,
                courseTitle: x.courseTitle,
                courseType: x.courseType,
                ects: x.ects,
                notes: x.notes,
                semester: x.semester,
                student: x.student,
                units: x.units,
                specialty: x.specialty.specialty,
                programGroup: x.programGroup.id ? x.programGroup.id : null,
                gradeYear: x.gradeYear && x.gradeYear.id ? x.gradeYear : null,
                gradePeriod: x.gradePeriod && x.gradePeriod.id ? x.gradePeriod : null,
                grade: x.formattedGrade ? currentGradeScale.convert(x.formattedGrade) : null,
                registrationType: x.registrationType ? 1 : 0,
              };
            });
            // and submit
            Promise.all(submissionData).then(res => {
              this._context.model('StudentCourses').save(res).then(async() => {
                this.filters.next(this.filters.getValue());
                this._loadingService.hideLoading();
                observer.next();
              }).catch((err) => {
                this._loadingService.hideLoading();
                observer.error(err);
              });
            }).catch(err => {
              this._loadingService.hideLoading();
              observer.error(err);
              console.log(err);
            })
          });
        })()
      }
    });
    this._loadingService.hideLoading();
  }

  async editCourses() {
    const student = (await this._context.model('Students')
      .where('id').equal(this._activatedRoute.snapshot.params.id).select('studyProgram, specialtyId').getItem());
    this._advancedFilter.values = {...this._advancedFilter.values,
      "student": this._activatedRoute.snapshot.params.id,
      "program": student.studyProgram,
      "specializationIndex": student.specialtyId
    }
    this._modalService.openModalComponent(StudentsCoursesExemptionComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: false,
      initialState: {
        studyProgram: student.studyProgram,
        student: this._activatedRoute.snapshot.params.id,
        modalTitle: 'StudyPrograms.EditCourses',
        execute: this.executeAction()
      }
    });
  }

  executeAction() {
    return new Observable((observer) => {
      this._loadingService.showLoading();
      // get add courses component
      const component = <StudentsCoursesExemptionComponent>this._modalService.modalRef.content;
      // and submit
      this._context.model('StudentCourses').save(Array.from(component.items)).then(async() => {
        this.filters.next(this.filters.getValue());
        this._loadingService.hideLoading();
        delete this._advancedFilter.values['student'];
        delete this._advancedFilter.values['program'];
        delete this._advancedFilter.values['specializationIndex'];
        observer.next();
      }).catch((err) => {
        this._loadingService.hideLoading();
        observer.error(err);
      });
    });
  }


  async onActionEvent(event) {
    if (event.type === 'editCourse') {
      await this.editCourse(event.data);
    }
  }

  calculateSimpleGradeAverage(items: any[]) {
    try {
      const formattedItems = items.map(x => {
        return {
          ...x,
          courseStructureType: x['courseStructureType/id']
        }
      });
      const average = this._gradesService.getGradesSimpleAverage(formattedItems).average;
      return this.gradeScale.format(average);
    } catch(err) {
      console.error(err);
      return '-';
    }
  }

  calculateFormattedGradeAverage(items: any[]) {
    try {
      const formattedItems = items.map(x => {
        return {
          ...x,
          courseStructureType: x['courseStructureType/id']
        }
      });
      const average = this._gradesService.getGradesWeightedAverage(formattedItems).average;
      return this.gradeScale.format(average);
    } catch(err) {
      console.error(err);
      return '-';
    }

  }

  setPrimaryGroup(group) {
    this.group1 = group;
    this.group2 = undefined;
    if (group) {
      this.selectedGroups.next([this.group1]);
    } else {
      this.selectedGroups.next([]);
    }
  }

  setSecondaryGroup(group) {
    if (this.group1.property !== group.property) {
      this.group2 = group;
      this.selectedGroups.next([this.group1, this.group2]);
    }
  }

}

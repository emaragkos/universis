import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as STUDENTS_LIST_CONFIG from '../students-table/students-table.config.list.json';
import {TableConfiguration} from '@universis/ngx-tables';
import {cloneDeep, template} from 'lodash';
import {map, reduce} from 'rxjs/operators';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {TemplatePipe} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-root',
  templateUrl: './students-root.component.html',
  styleUrls: ['./students-root.component.scss'],
  providers: [TemplatePipe]
})
export class StudentsRootComponent implements OnInit, OnDestroy {
  public student: any;
  public studentID: any;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public isCreate = false;
  public allowedActions: any[];
  public edit: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _template: TemplatePipe
  ) {
  }

  async ngOnInit() {
    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentID = params.id;
      this.student = await this._context.model('Students')
        .where('id').equal(params.id)
        .expand('person, department, studyProgram')
        .getItem();

      // @ts-ignore
      this.config = cloneDeep(STUDENTS_LIST_CONFIG as TableConfiguration);

      if (this.config.columns && this.student) {
        // get actions from config file
        this.actions = this.config.columns.filter(x => {
          return x.actions;
        })
          // map actions
          .map(x => x.actions)
          // get list items
          .reduce((a, b) => b, 0);

        // filter actions with student permissions
        this.allowedActions = this.actions.filter(x => {
          if (x.role) {
            if (x.role === 'action') {
              if (x.access && x.access.length > 0) {
                let access = x.access;
                access = access.filter(y => {
                  if (y.studentStatuses) {
                    return y.studentStatuses.find(z => z.alternateName === this.student.studentStatus.alternateName);
                  }
                });
                if (access && access.length > 0) {
                  return x;
                }
              } else {
                return x;
              }
            }
          }
        });

        this.edit = this.actions.find(x => {
          if (x.role === 'edit') {
            x.href = this._template.transform(x.href, this.student);
            return x;
          }
        });

        this.actions = this.allowedActions;
        this.actions.forEach(action => {
          if (action.href.includes('${studyProgramId}')) {
            action.href = action.href.replace('${studyProgramId}', '${studyProgram.id}');
          }
          action.href = this._template.transform(action.href, this.student);
        });

        // save user activity for student
        return this._userActivityService.setItem({
          category: this._translateService.instant('Students.StudentTitle'),
          description: this._translateService.instant(this.student.person.givenName + ' ' + this.student.person.familyName),
          url: window.location.hash.substring(1),
          dateCreated: new Date()
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

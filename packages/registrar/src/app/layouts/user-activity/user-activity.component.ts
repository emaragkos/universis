import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserActivityEntry, UserActivityService } from '@universis/common';


/**
 *
 * UserActivityComponent
 *
 * Uses the user activity service to list the last actions of the user.
 * These actions are provided
 *
 */
@Component({
  selector: 'app-user-activity',
  templateUrl: './user-activity.component.html'
})
export class UserActivityComponent implements OnInit, OnDestroy {

  private userActionsSubscription: Subscription;

  public userActionsList: Array<UserActivityEntry> = [];

  constructor(
    private _userActivityService: UserActivityService
  ) { }

  async ngOnInit() {
    this.userActionsSubscription =
    (await this._userActivityService.getItemsAsObservable())
      .subscribe((userActionsList: Array<UserActivityEntry>) => {
        this.userActionsList = userActionsList;
      });
  }

  ngOnDestroy(): void {
    if (this.userActionsSubscription != null) {
      this.userActionsSubscription.unsubscribe();
    }
  }
}

import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActiveDepartmentService } from "../../../registrar-shared/services/activeDepartmentService.service";

@Component({
  selector: 'app-register-latest-messages',
  templateUrl: './latest-messages.component.html'
})
export class LatestMessagesComponent implements OnInit, OnDestroy {
  ngOnDestroy(): void {
    if (this.timeoutInterval) {
      clearInterval(this.timeoutInterval);
    }
  }

  @Input() items: any;
  private department: any;
  private timeoutInterval: any;

  constructor(private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService) { }
  ngOnInit() {
    this._activeDepartmentService.getActiveDepartment().then((activeDepartment) => {
      this.department = activeDepartment;
      this.fetch();
      this.timeoutInterval = setInterval(() => {
        this.fetch()
      }, 60000);
    });
  }

  fetch() {
    this._context.model('StudyProgramRegisterActionMessages')
      .where('recipient/name').equal('Registrar')
      .and('action/studyProgram/department').equal(this.department && this.department.id)
      .orderByDescending('dateCreated')
      .expand('sender')
      .take(5)
      .getItems().then((results) => {
        this.items = results;
      });
  }

}

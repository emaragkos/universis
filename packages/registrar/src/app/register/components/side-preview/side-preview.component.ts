import {Component, Input, OnInit} from '@angular/core';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-candidate-side-preview',
  templateUrl: './side-preview.component.html'
})
export class SidePreviewComponent implements OnInit {

  @Input() model;

  public currentLang: any;

  constructor(private _configurationService: ConfigurationService) { }

  ngOnInit() {
    this.currentLang = this._configurationService.currentLocale;
  }

}

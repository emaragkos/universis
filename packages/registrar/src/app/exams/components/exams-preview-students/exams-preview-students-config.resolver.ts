import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class ExamsStudentsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-students-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./exams-students-table.config.list.json`);
        });
    }
}

export class ExamsStudentsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./exams-students-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./exams-students-table.search.list.json`);
            });
    }
}

export class ExamsStudentsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./exams-students-table.config.list.json`);
    }
}

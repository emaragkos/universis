import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AngularDataContext } from '@themost/angular';
import * as EXAMS_STUDENTS_LIST_CONFIG from './exams-participations-table.config.list.json';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';

@Component({
  selector: 'app-exams-preview-participations',
  templateUrl: './exams-preview-participations.component.html'
})
export class ExamsPreviewParticipationsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> EXAMS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  public examId: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  // @Input() tableConfiguration: any;
  // @Input() searchConfiguration: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _context: AngularDataContext,
    private _activeDepartmentService: ActiveDepartmentService
  ) { }

  async ngOnInit() {
    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.examId = params.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('CourseExamParticipateActions')
        .where('courseExam')
        .equal(this.examId)
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(EXAMS_STUDENTS_LIST_CONFIG);
      this.students.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, { department: activeDepartment });
          });
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

}

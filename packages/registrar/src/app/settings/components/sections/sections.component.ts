import { Component, OnInit, OnDestroy } from '@angular/core';
import { SettingsService, SettingsSection } from '../../../settings-shared/services/settings.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-settings-sections',
  templateUrl: './sections.component.html'
})
export class SectionsComponent implements OnInit, OnDestroy {

  public sections: SettingsSection[];
  public search: string = null;
  subscription: Subscription;

  constructor(private _settings: SettingsService) { }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.subscription = this._settings.sections.subscribe( sections => {
      this.sections = sections.sort((a, b) => {
        if (a.description > b.description) {
          return 1;
        }
        if (a.description < b.description) {
          return -1;
        }
        return 0;
      });
    });
  }

}

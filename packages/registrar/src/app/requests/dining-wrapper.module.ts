import {AdminDiningModule} from '@universis/ngx-dining/admin';
import {NgModule} from '@angular/core';

@NgModule({
  imports: [AdminDiningModule]
})
export class DiningWrapperModule {
}

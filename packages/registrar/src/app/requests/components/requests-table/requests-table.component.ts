import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import * as REQUESTS_LIST_CONFIG from './requests-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '@universis/ngx-tables';
import {ActivatedRoute, Router} from '@angular/router';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {Observable, Subscription} from 'rxjs';
import {AppEventService, ErrorService, LoadingService, ModalService, UserActivityService, UserService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';
import {ClientDataQueryable, ResponseError} from '@themost/client';
import {RequestActionComponent} from '../request-action/request-action.component';
import {ReportService} from '../../../reports-shared/services/report.service';
import { DocumentSignActionComponent, SignerService } from '@universis/ngx-signer';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {DocumentDownloadComponent} from '../../../reports-shared/components/document-download/document-download.component';

interface RequestDocumentActionSelectedRow {
  id: number;
  actionStatus: string;
  published?: boolean;
  datePublished?: any;
  signReport?: boolean;
  signed?: boolean;
  reportTemplate?: number;
  student?: number;
  documentNumber?: string;
  resultUrl?: string;
  parentDocumentSeries?: number;
  additionalType?: string;
  agent?: string;
}

@Component({
  selector: 'app-requests-table',
  templateUrl: './requests-table.component.html'
})
export class RequestsTableComponent implements OnInit, OnDestroy {

  public readonly config = REQUESTS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  private changeSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @ViewChild('downloadComponent') downloadComponent: DocumentDownloadComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public showPublish = true;
  public showSign = true;
  public documentsToPrint: any[];
  public showPdfViewer = false;

  private selectedItems = [];
  private queryParamsSubscription: Subscription;

  constructor(
    private _context: AngularDataContext,
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _activatedTable: ActivatedTableService,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _appEvent: AppEventService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _reports: ReportService,
    private _signer: SignerService,
    private _userService: UserService,
    private _httpClient: HttpClient
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        // show or hide publish action
        this.showPublish = this.table.config.columns .findIndex( (column) => {
          return column.property === 'published';
        }) >= 0;
        // show or hide sign action
        this.showSign = this.table.config.columns .findIndex( (column) => {
          return column.property === 'signed';
        }) >= 0;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Requests.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });

    this.changeSubscription = this._appEvent.change.subscribe((event) => {
      if  (this.table.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === 'DocumentNumberSeriesItems') {
        this.table.fetchOne({
          result: event.target.id
        });
      }
      if (event && event.target && event.model === this.table.config.model) {
        this.table.fetchOne({
          id: event.target.id
        });
      }
    });

    this.queryParamsSubscription = this._activatedRoute.queryParams.subscribe(async(queryParams) => {
      if (queryParams.download) {
        this._loadingService.showLoading()
        this.documentsToPrint = [{blob: await this.downloadComponent.download(queryParams.download)}];
        this.showPdfViewer = true;
        this._loadingService.hideLoading();
      } else {
        if (this.showPdfViewer){
          this.showPdfViewer = false;
          this.downloadComponent.closeViewer();
        }
      }
    })
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      // search for document attributes (if table has published column)
      const hasPublishedColumn = this.table.config.columns.find( (col) => {
        return col.property === 'published';
      });
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus', 'additionalType'];
          if (hasPublishedColumn) {
            selectArguments.push('result/id as result',
                'object/reportTemplate/signReport as signReport',
                'result/signed as signed',
                'result/published as published',
                'result/datePublished as datePublished',
                'object/reportTemplate/id as reportTemplate',
              'student/id as student',
              'result/documentNumber as documentNumber',
              'result/url as resultUrl',
              'result/documentCode as documentCode',
              'result/parentDocumentSeries as parentDocumentSeries');
          }
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(-1)
              .skip(0)
              .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            if (hasPublishedColumn) {
              return <RequestDocumentActionSelectedRow>{
                id: item.id,
                actionStatus: item.actionStatus,
                result: item.result,
                signed: item.signed,
                signReport: item.signReport,
                published: item.published,
                datePublished: item.datePublished,
                reportTemplate: item.reportTemplate,
                documentNumber: item.documentNumber,
                student: item.student,
                resultUrl: item.resultUrl,
                parentDocumentSeries: item.parentDocumentSeries,
                additionalType: item.additionalType,
                agent: item.agentName
              };
            } else {
              return {
                id: item.id,
                actionStatus: item.actionStatus
              };
            }

          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executeSignActionOne(item, index) {
    return new Promise((resolve, reject) => {
      // set progress
      const total = this.selectedItems.length;
      this._context.model(this.table.config.model)
          .where('id').equal(item.id)
          .select('result')
          .expand('result')
          .getItem().then((res) => {
        if (res == null) {
          return reject(new Error('The specified item cannot be found'));
        }
        // set component item which is the current document
        if (res.result == null) {
          return reject(new Error('The specified document cannot be found'));
        }
        const component = <DocumentSignActionComponent>this._modalService.modalRef.content;
        const document = res.result;
        setTimeout(() => {
          // sign document
          this.refreshAction.emit({
            progress: Math.floor(((index + 1) / total) * 100)
          });
          component.sign(document).then(() => {
            // log success
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
          }).catch((err) => {
            // log error
            return resolve({
              success: 0,
              error: err
            });
          });
        }, 750);
      }).catch((err) => {
        // log error
        console.log(err);
        return resolve({
          success: 0,
          error: err
        });
      });
    });
  }

  executeSignAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeSignActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  executePublishAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      const datePublished = new Date();
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.result,
          reqId: item.id,
          published: true,
          datePublished: datePublished
        };
      });
      // execute promises in series within an async method
      (async () => {
        for (let i = 0 ; i < updated.length ; i++) {
          const item = updated[i];
          const res: any = await this._executePublishActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        // make animation a bit smoother
        setTimeout(() => {
          observer.next(result);
        }, 500);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executePublishActionOne(item, index) {
    const total = this.selectedItems.length;
    return new Promise((resolve, reject) => {
      const reqId = item.reqId;
      delete item.reqId;
      this._context.model('DocumentNumberSeriesItems').save(item).then(() => {
        setTimeout(() => {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            return this.table.fetchOne({
              id: reqId
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
        }, 500);
      }).catch(err => {
        // log error
        console.error(err);
        return resolve({
          success: 0,
          error: err
        });
      });
    });
  }

  /***
   * Accepts the selected requests
   */
  async acceptAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.AcceptAction.Title',
          description: 'Requests.Edit.AcceptAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CompletedActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Publishes the selected requests
   */
  async publishAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return (item.actionStatus === 'CompletedActionStatus') &&
            (!!item.published === false) &&
            ((!!item.signReport === false) || (!!item.signReport === true && item.signed === true));
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.PublishAction.Title',
          modalIcon: 'far fa-newspaper mt-0',
          description: 'Requests.Edit.PublishAction.Description',
          refresh: this.refreshAction,
          execute: this.executePublishAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Rejects the selected requests
   */
  async rejectAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active items
      this.selectedItems = items.filter( (item) => {
        return item.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.RejectAction.Title',
          description: 'Requests.Edit.RejectAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeActionStatus('CancelledActionStatus')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async signAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'CompletedActionStatus' &&
            item.result != null &&
            item.signed === false;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(DocumentSignActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          item: {
          },
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.SignAction.Title',
          description: 'Requests.Edit.SignAction.Description',
          refresh: this.refreshAction,
          execute: this.executeSignAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async claimAction() {
    try {
      this._loadingService.showLoading();
      // get only active items
      this.selectedItems = await this.getSelectedItems();
      const items = this.selectedItems.filter((x: RequestDocumentActionSelectedRow) => {
        return x.actionStatus === 'ActiveActionStatus' && !x.agent;
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: items,
          modalTitle: 'Requests.Edit.ClaimAction.Title',
          description: 'Requests.Edit.ClaimAction.Description',
          refresh: this.refreshAction,
          execute: this.executeClaimAction(this.selectedItems)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeClaimAction(requests: Array<RequestDocumentActionSelectedRow>) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = requests.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      (async () => {
        if (requests && Array.isArray(requests)) {
          for (const request of requests) {
            const index = requests.indexOf(request);
            if (request) {
              const schema = await this._context.getMetadata();
              const user = await this._userService.getUser();
              const item = {
                id: request.id,
                agent: {
                  id: user.id,
                  name: user.name
                }
              };
              const findEntitySet = schema.EntityContainer.EntitySet.find((entitySet) => {
                return entitySet.EntityType === request.additionalType;
              });
              if (findEntitySet == null) {
                throw new ResponseError('The specified entity set cannot be found', 409);
              }
              const res = await this._context.model(findEntitySet.Name).where('id').equal(request.id).getItem();
              if (res && res.agent && res.agent.id === user.id) {
                return;
              }
              this._context.model(findEntitySet.Name).save(item).then(async () => {
                Object.assign(request, {
                  agent: {
                    id: user.id,
                    name: user.name
                  }
                });
                try {
                  this.refreshAction.emit({
                    progress: Math.floor(((index + 1) / total) * 100)
                  });
                  await this.table.fetchOne({
                    id: request.id
                  });
                } catch (err) {
                  console.log(err);
                  this._errorService.showError(err, {
                    continueLink: '.'
                  });
                }
              }).catch(err => {
                console.log(err);
                result.errors++;
                this._errorService.showError(err, {
                  continueLink: '.'
                });
              });
            }
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  private _executeReissueActionOne(item: RequestDocumentActionSelectedRow, index: number) {

    const total = this.selectedItems.length;
    return new Promise((resolve, reject) => {
      this._reports.printReport(item.reportTemplate, {
        ID: item.student,
        REPORT_USE_DOCUMENT_NUMBER: false,
        REPORT_DOCUMENT_SERIES: item.parentDocumentSeries,
        REPORT_DOCUMENT_NUMBER: item.documentNumber
      }).then((blob) => {
        // replace document
        const updateItem = {
          url: item.resultUrl,
          published: false,
          datePublished: null,
          signed: false
        };
        return this._signer.replaceDocument(updateItem, blob).then(() => {
          setTimeout(() => {
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            return this.table.fetchOne({
              id: item.id
            }).then(() => {
              return resolve({
                success: 1
              });
            }).catch((err) => {
              console.log(err);
              return resolve({
                success: 1,
                error: err
              });
            });
          }, 500);
        });
      }).catch((err) => {
        return resolve({
          success: 0,
          error: err
        });
      });
    });

  }

  executeReissueAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let i = 0; i < this.selectedItems.length; i++) {
          const item = this.selectedItems[i];
          const res: any = await this._executeReissueActionOne(item, i);
          result.success += res.success;
          result.errors += 1 - res.success;
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  /**
   *
   * Downloads the attachments of the selected rows and calls the print component
   *
   */
  async executePrintAction(): Promise<void> {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item) => {
        return item.actionStatus === 'CompletedActionStatus' &&
            item.result != null
      });

      const headers = this._context.getService().getHeaders();
      const rawQueries = this.selectedItems.map(item => {
        const documentURL = this._context.getService().resolve(item.resultUrl.replace(/\\/g, '/').replace(/^\/api\//, ''));
        return this._httpClient.get(documentURL, {
          headers,
          responseType: 'blob',
          observe: 'response'
        }).toPromise();
      });

      const result = (await Promise.all(rawQueries)).map(item => item.body).map((item => ({blob: item})));
      this.documentsToPrint = result;
      this.showPdfViewer = true;
    } catch(err) {
      console.error(err);
    } finally {
      this._loadingService.hideLoading();
    }
  }

  /**
   * Publishes the selected requests
   */
  async reissueAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item: RequestDocumentActionSelectedRow) => {
        return ((item.actionStatus === 'CompletedActionStatus') &&
          (item.published === false) &&
          (item.resultUrl != null)
        );
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Requests.Edit.ReissueAction.Title',
          modalIcon: 'far fa-print mt-0',
          description: 'Requests.Edit.ReissueAction.Description',
          refresh: this.refreshAction,
          execute: this.executeReissueAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
    if (this.queryParamsSubscription){
      this.queryParamsSubscription.unsubscribe();
    }
  }

  async releaseAction() {
    try {
      this._loadingService.showLoading();
      // get only active items
      this.selectedItems = await this.getSelectedItems();
      const items = this.selectedItems.filter((x: RequestDocumentActionSelectedRow) => {
        return x.actionStatus === 'ActiveActionStatus';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: items,
          modalTitle: 'Requests.Edit.ReleaseAction.Title',
          description: 'Requests.Edit.ReleaseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeReleaseAction(this.selectedItems)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private executeReleaseAction(requests: Array<RequestDocumentActionSelectedRow>) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = requests.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      (async () => {
        if (requests && Array.isArray(requests)) {
          for (const request of requests) {
            const index = requests.indexOf(request);
            if (request) {
              const schema = await this._context.getMetadata();
              const item = {
                id: request.id,
                agent: null
              };
              const findEntitySet = schema.EntityContainer.EntitySet.find((entitySet) => {
                return entitySet.EntityType === request.additionalType;
              });
              if (findEntitySet == null) {
                throw new ResponseError('The specified entity set cannot be found', 409);
              }
              this._context.model(findEntitySet.Name).save(item).then(async (res) => {
                Object.assign(request, res);
                try {
                  this.refreshAction.emit({
                    progress: Math.floor(((index + 1) / total) * 100)
                  });
                  await this.table.fetchOne({
                    id: request.id
                  });
                } catch (err) {
                  console.log(err);
                  this._errorService.showError(err, {
                    continueLink: '.'
                  });
                }
              }).catch(err => {
                console.log(err);
                result.errors++;
                this._errorService.showError(err, {
                  continueLink: '.'
                });
              });
            }
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        result.errors++;
        observer.error(err);
      });
    });
  }
}

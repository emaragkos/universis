import {Component, OnInit, OnDestroy, Input, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {
  DiagnosticsService,
  DIALOG_BUTTONS,
  ErrorService,
  LoadingService,
  ModalService,
  RequestNotFoundError,
  ToastService
} from '@universis/common';
import {Subscription} from 'rxjs';
import {RequestsService} from '../../../services/requests.service';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AppEventService} from '@universis/common';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '@universis/ngx-tables';
import * as DOCUMENT_REQUESTS_LIST_CONFIG from './student-request-documents-table.config.json';
import {ActivatedTableService} from '@universis/ngx-tables';

@Component({
  selector: 'app-preview-graduation-request-action',
  templateUrl: './preview.component.html',
  styles: [`
        .flex-grow-3 {
          flex-grow: 3;
        }
    `]
})
export class GraduationRequestActionPreviewComponent implements OnInit, OnDestroy {

  public data: any;
  public loading = true;
  public requiredAttachmentRule = false;
  public graduationRule: false;
  public declaredActions: any;
  public graduationDegree: any;
  public graduateActions: any;
  @ViewChild('requests') requests: AdvancedTableComponent;
  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>DOCUMENT_REQUESTS_LIST_CONFIG;
  public allowDeclare = true;

  public requestValidations: any;
  public responseModel = {
    body: null,
    attachment: null,
    subject: null
  };


  constructor(private _context: AngularDataContext,
              private _route: ActivatedRoute,
              private _router: Router,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _activatedTable: ActivatedTableService,
              private _diagnosticsService: DiagnosticsService,
              private _requestsServices: RequestsService) {
  }

  @Input() request: any;
  private changeSubscription: Subscription;
  public requestPeriodExists = false;

  async ngOnInit() {
    this.load();
    this.changeSubscription = this._appEvent.change.subscribe(async (event) => {
      if (event && (event.model === 'GraduationRequestActions' || event.model === 'StudentRequestActions')
        && event.target && this.request && event.target.id === this.request.id) {
        // reload
        await this.load();
      }
    });
  }

  private async load() {
    this.loading = true;
    this._loadingService.showLoading();

    // get supplied documents
    const attachments = this.request.attachments || [];
    // check required attachments
    for (let i = 0; i < this.request.graduationEvent.attachmentTypes.length; i++) {
      const attachmentType = this.request.graduationEvent.attachmentTypes[i];
      attachmentType.attachment = attachments.find(x => {
        return x.attachmentType === attachmentType.attachmentType.id;
      });
    }
    this.data = this.request;

    // check if exist graduationAvailableEvent for this student
    const availableGraduationEvents = await this._requestsServices.getAvailableGraduationEvent(this.request.student);
    const availableGraduationEvent = availableGraduationEvents.find(x => {
      return x.id === this.request.graduationEvent.id;
    });
    this.requestPeriodExists = availableGraduationEvent ? true : false;

    if (this.data.actionStatus.alternateName !== 'PotentialActionStatus') {
      // get related graduate actions
      // get related student declare action
      this.graduateActions = await this._context.model('StudentGraduateActions')
        .where('initiator').equal(this.data.id)
        .expand('actionStatus').getItems();
      // get related student declare action
      if (this.graduateActions && this.graduateActions.length === 0) {
        this.declaredActions = await this._context.model('StudentDeclareActions')
          .where('initiator').equal(this.data.id)
          .and('actionStatus/alternateName').notEqual('CancelledActionStatus')
          .orderByDescending('dateCreated')
          .expand('actionStatus').getItems();
      }
      // get services from api to check if student status listener is enabled
      // Some institutes allow approval of graduation request only to declared students (not active)
      const services = await this._context.model('diagnostics/services').getItems();
      if (services && services.length) {
        this.allowDeclare = !services.find(x => {
          return x.serviceType === 'ValidateGraduationRequestStudentStatus';
        });
      }
      await this.validateRequest();
      // get related requestDocumentActions
      if (this.graduateActions && this.graduateActions.length > 0) {
        this.getDocumentRequests();
      }
    }
    this.loading = false;
    this._loadingService.hideLoading();
  }

  downloadFile(attachment) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  async validateRequest() {
    try {
      // validate request
      this.requestValidations = await this._context.model(`GraduationRequestActions/${this.data.id}/validate`).getItems();
      if (this.requestValidations && this.requestValidations.validationResults) {
        this.requiredAttachmentRule = (this.requestValidations.validationResults || []).find(x => {
          return x.type === 'RequiredAttachmentRule' && x.success === true;
        });
        this.graduationRule = (this.requestValidations.validationResults || []).find(x => {
          return x.type === 'GraduationRule' && x.success === true;
        });
        if (this.graduationRule && this.data && this.data.actionStatus && this.data.actionStatus.alternateName === 'ActiveActionStatus') {
          // get calculated graduation degree
          this.graduationDegree = await this._context.model(`Students/${this.data.student}/calculateGraduationDegree`).save({});
        }
      }
    } catch (err) {
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  ngOnDestroy(): void {
    if (this.changeSubscription) {
      this.changeSubscription.unsubscribe();
    }
  }

  async addVirtualAttachment(attachmentType) {
    try {
      this._loadingService.showLoading();
      const attachment = {
        'attachmentType': attachmentType.attachmentType.id
      };
      const result = await this._context.model(`StudentRequestActions/${this.data.id}/addVirtualAttachment`).save(attachment);
      attachmentType.attachment = result;
      this.changeAttachmentStatus(result, 'CompletedActionStatus');
      await this.validateRequest();
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async removeVirtualAttachment(attachmentType) {
    try {
      this._loadingService.showLoading();
      const attachment = {
        'id': attachmentType.attachment.id
      };
      // remove virtual attachment
      const result = await this._context.model(`StudentRequestActions/${this.data.id}/removeVirtualAttachment`).save(attachment);
      attachmentType.attachment = result;
      await this.validateRequest();
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async changeAttachmentStatus(attachment, status) {
    try {
      this._loadingService.showLoading();
      // try to get action
      let action = await this._context.model('AcceptAttachmentActions')
        .where('object').equal(attachment.id)
        .getItem();
      // validate action state
      if (action == null) {
        action = {
          object: attachment.id, // set attachment as object
        };
      }
      // set action status to complete
      action.actionStatus = {
        alternateName: status
      };
      // and finally save action
      const result = await this._context.model('AcceptAttachmentActions').save(action);
      // add result to attachment action
      if (result) {
        const attachmentType = this.data.graduationEvent.attachmentTypes.find(x => {
          return x.attachment && x.attachment.id === attachment.id;
        });
        if (attachmentType && attachmentType.attachment) {
          if (attachmentType.attachment.actions && attachmentType.attachment.actions.length > 0) {
            attachmentType.attachment.actions[0] = result;
          } else {
            // add action
            attachmentType.attachment.actions = [];
            attachmentType.attachment.actions.push(result);
          }
        }
      }
      const actionDescription = status === 'CancelledActionStatus' ? 'Reject' : 'Accept';
      this._toastService.show(
        this._translateService.instant(`Documents.${actionDescription}`),
        this._translateService.instant(`Documents.${actionDescription}Message`)
      );
      await this.validateRequest();
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async changeDeclareActionStatus(declareAction: any, status) {
    const previousActionStatus = declareAction.actionStatus;
    try {
      const title = status === 'CompletedActionStatus' ?
        this._translateService.instant('GraduationRequestActions.DeclareStudent') :
        this._translateService.instant('GraduationRequestActions.CancelDeclareStudent');
      const message = status === 'CompletedActionStatus' ?
        this._translateService.instant('GraduationRequestActions.DeclareStudentMessage') :
        this._translateService.instant('GraduationRequestActions.CancelDeclareStudentMessage');

      const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();
      // set action status
      declareAction.actionStatus = {
        alternateName: status
      };
      // and finally save action
      await this._context.model('StudentDeclareActions').save(declareAction);
      // add result to attachment action
      const actionDescription = status === 'CancelledActionStatus' ? 'Cancel' : '';
      this._toastService.show(
        this._translateService.instant(`GraduationRequestActions.${actionDescription}CompleteDeclareStudent`),
        this._translateService.instant(`GraduationRequestActions.${actionDescription}CompleteDeclareStudent`)
      );

      this._appEvent.change.next({
        model: 'StudentRequestActions',
        target: this.request
      });

      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      // reset status
      declareAction.actionStatus = {
        alternateName: previousActionStatus.alternateName
      };
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async changeGraduateActionStatus(graduateAction: any, status) {
    const previousActionStatus = graduateAction.actionStatus;
    try {
      const title = status === 'CompletedActionStatus' ?
        this._translateService.instant('GraduationRequestActions.GraduateStudent') :
        this._translateService.instant('GraduationRequestActions.CancelGraduateStudent');
      const message = status === 'CompletedActionStatus' ?
        this._translateService.instant('GraduationRequestActions.GraduateStudentMessage') :
        this._translateService.instant('GraduationRequestActions.CancelGraduateStudentMessage');

      const dialogResult = await this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo);
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();
      // set action status
      graduateAction.actionStatus = {
        alternateName: status
      };
      // and finally save action
      await this._context.model('StudentGraduateActions').save(graduateAction);
      // add result to attachment action
      const actionDescription = status === 'CancelledActionStatus' ? 'Cancel' : '';
      this._toastService.show(
        this._translateService.instant(`GraduationRequestActions.${actionDescription}CompleteGraduateStudent`),
        this._translateService.instant(`GraduationRequestActions.${actionDescription}CompleteGraduateStudent`)
      );
      this._appEvent.change.next({
        model: 'StudentRequestActions',
        target: this.request
      });

      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      // reset status
      graduateAction.actionStatus = {
        alternateName: previousActionStatus.alternateName
      };
      return this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  getDocumentRequests() {
    this._activatedTable.activeTable = this.requests;
    this.requests.query = this._context.model('RequestDocumentActions')
      .where('initiator').equal(this.data.id)
      .prepare();
    this.requests.config = AdvancedTableConfiguration.cast(DOCUMENT_REQUESTS_LIST_CONFIG);
    this.requests.ngOnInit();
  }

  /**
   * changeToPotential an action
   */
  async changeToPotential(sendMessage?: boolean) {
    const title = this._translateService.instant('GraduationRequestActions.PotentialAction');
    const message = this._translateService.instant('GraduationRequestActions.ModalAcceptTitle');
    this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
      if (dialogResult === 'no') {
        return;
      }
      this._loadingService.showLoading();
      const item = {
        id: this.request.id,
        actionStatus: {
          alternateName: 'PotentialActionStatus'
        }
      };
      this._context.getMetadata().then((schema) => {
        // get entity set
        const entitySet = schema.EntityContainer.EntitySet.find((x) => x.EntityType === this.request.additionalType);
        return this._context.model(entitySet.Name).save(item).then((result) => {
          if (sendMessage) {

            this.responseModel.subject = this._translateService.instant(`GraduationRequestActions.MessageMailSubject`);

            let body = '';
            if(this.requestValidations.validationResults){
              (this.requestValidations.validationResults || []).find(x => {
                if(x.type === 'RequiredAttachmentRule')
                {
                  x.attachmentTypes.find( z=> {
                    if (!z.validationResult.success){
                        body += this._translateService.instant('GraduationRequestActions.NoValidAttachment', {name : z.attachmentType.name});
                    }
                  });
                }
              });
            }
            this.responseModel.body = body;

            this.sendMessage().then(() => {
              // do refresh
              Object.assign(this.request, result);
              ///// this.load(this.request.id);
              // send application event
              this._appEvent.change.next({
                model: this.request.additionalType,
                target: this.request
              });
              this._loadingService.hideLoading();
            }).catch((err) => {
              Object.assign(this.request, result);
              ///// this.load(this.request.id);
              this._loadingService.hideLoading();
              // show error for failed message
              const errorTitle = this._translateService.instant('Modals.FailedSendMessageTitle');
              const errorMessage = this._translateService.instant('Modals.FailedSendMessageMessage');
              this._loadingService.hideLoading();
              return this._modalService.showErrorDialog(errorTitle, errorMessage);
            });
          } else {
            // do refresh
            Object.assign(this.request, result);
            ///// this.load(this.request.id);
            // send application event
            this._appEvent.change.next({
              model: entitySet.Name,
              target: this.request
            });
            this._loadingService.hideLoading();
          }
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        console.log(err);
        const errorTitle = this._translateService.instant('GraduationRequestActions.AcceptRequestFailed');
        return this._modalService.showWarningDialog(errorTitle, err.error ? err.error.message : err.message);
      });
    });
  }

  private async sendMessage(): Promise<any> {
    // send message
    await this._requestsServices.sendResponse(
      this.request.student,
      this.request.id,
      this.responseModel
    ).toPromise();
    // get messages and refresh
    this.request.messages = await this._context.model(`StudentRequestActions/${this.request.id}/messages`)
      .asQueryable()
      .expand('attachments')
      .orderByDescending('dateCreated')
      .getItems();
    // clear form
    this.responseModel = {
      body: null,
      attachment: null,
      subject: null
    };
  }



}
@Component({
  selector: 'app-graduation-request-action-container',
  template: `
    <div *ngIf="request">
      <app-preview-graduation-request-action [request]="request"></app-preview-graduation-request-action>
    </div>
  `
})

// tslint:disable-next-line:component-class-suffix
export class GraduationRequestActionContainer implements OnInit, OnDestroy {
  public request;
  private dataSubscription: Subscription;

  constructor(private _requestService: RequestsService,
              private _activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _context: AngularDataContext) {
    //
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.dataSubscription = this._activatedRoute.params.subscribe((params) => {
       this._context.model('GraduationRequestActions').where('id').equal(params.id)
        .expand('attachments($expand=actions($expand=actionStatus))',
          'graduationEvent($expand=graduationYear,' +
          'graduationPeriod,' +
          'attachmentTypes($expand=attachmentType))')
         .getItem().then((item) => {
        this.request = item;
      }).catch((err) => {
        return this._errorService.navigateToError(err);
      });
    }, (err) => {
      return this._errorService.navigateToError(err);
    });
  }
}

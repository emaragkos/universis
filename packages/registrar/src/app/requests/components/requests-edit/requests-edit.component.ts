import {Component, EventEmitter, OnDestroy, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {RequestsService} from '../../services/requests.service';
import {ActivatedRoute, Router} from '@angular/router';
import {DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService, UserService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ActionButtonItem} from '../../../registrar-shared/actions-button/actions-button.component';
import {AngularDataContext} from '@themost/angular';
import {ResponseError} from '@themost/client';
import {Subscription} from 'rxjs';
import {AppEventService} from '@universis/common';

@Component({
  selector: 'app-requests-edit',
  templateUrl: './requests-edit.component.html',
  styleUrls: ['./requests-edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RequestsEditComponent implements OnInit, OnDestroy {

  public request: any;
  public actions: any[];
  public defaultAction: any;
  public initiatorOf: any;
  public currentUser: any;
  public claimed = false;
  private fragmentSubscription: Subscription;

  private refresh = new EventEmitter<any>(null);
  private refreshSubscription: Subscription;
  private paramsSubscription: Subscription;
  private reloadSubscription: Subscription;

  @ViewChild('file') file;

  public responseModel = {
    body: null,
    attachment: null,
    subject: null
  };

  constructor(private _requestsService: RequestsService,
              private _route: ActivatedRoute,
              private _router: Router,
              private _errorService: ErrorService,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _translate: TranslateService,
              private _context: AngularDataContext,
              private _userService: UserService,
              private _translateService: TranslateService,
              private _toastService: ToastService,
              private _appEvent: AppEventService,
              private _activatedRoute: ActivatedRoute) { }

  private readonly SendMessageAction = 'Requests.Edit.SendMessage';
  private readonly AcceptAction = 'Requests.Edit.accept';
  private readonly RejectAction = 'Requests.Edit.reject';
  private readonly ReleaseAction = 'Requests.Edit.release';

  ngOnInit() {

    this.actions = <Array<ActionButtonItem>> [
      // {
      //   title: this.SendMessageAction,
      //   disabled: true,
      //   click: () => {
      //     //
      //   }
      // },
      {
        title: this.AcceptAction,
        disabled: true,
        click: () => {
          return this.accept();
        }
      },
      {
        title: this.RejectAction,
        disabled: true,
        click: () => {
          return this.reject();
        }
      },
      {
        title: this.ReleaseAction,
        disabled: true,
        click: () => {
          return this.release();
        }
      }
    ];
    // get request
    this.paramsSubscription = this._route.parent.params.subscribe(routeParams => {
      this.load(routeParams.id);
    });

    // get request
    this.reloadSubscription = this._appEvent.change.subscribe(event => {
      if (event && event.target && event.model === 'StudentRequestActions') {
        // reload request
        if (event.target.id === this.request.id) {
          this.load(event.target.id);
        }
      }
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.load(this.request.id);
      }
    });
    // refresh component
    this.refreshSubscription = this.refresh.subscribe((result) => {
      if (result) {
        // validate defaultAction
        // enable or disable action
        const active = result.actionStatus.alternateName === 'ActiveActionStatus';
        this.claimed = this.currentUser && result.agent != null && result.agent.name === this.currentUser.name;
        if (result.agent == null) {
          this.defaultAction = <ActionButtonItem> {
            title: 'Requests.Edit.claim',
            disabled: (active === false),
            click: () => {
              return this.claim();
            }
          };
        } else {
          this.defaultAction = <ActionButtonItem> {
            title: 'Requests.Edit.accept',
            disabled: (active === false) || (this.claimed === false),
            click: () => {
              return this.accept();
            }
          };
        }
        // this.actions.find((x) => x.title === this.SendMessageAction).disabled = (active === false);
        this.actions.find((x) => x.title === this.AcceptAction).disabled = (active === false) || (this.claimed === false);
        this.actions.find((x) => x.title === this.RejectAction).disabled = (active === false) || (this.claimed === false);
        this.actions.find((x) => x.title === this.ReleaseAction).disabled = (active === false) || (result.agent == null);
      }
    });

  }

  ngOnDestroy(): void {
    if (this.paramsSubscription) {
      this.paramsSubscription.unsubscribe();
    }
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  /**
   * Current user claims the action
   */
  public claim() {
    if (this.request) {
      this._loadingService.showLoading();
      this._context.getMetadata().then((schema) => {
        return this._userService.getUser().then((user) => {
          const item = {
            id: this.request.id,
            agent: {
              id: user.id,
              name: user.name
            }
          };
          // get entity set
          const findEntitySet = schema.EntityContainer.EntitySet.find((entitySet) => {
            return entitySet.EntityType === this.request.additionalType;
          });
          if (findEntitySet == null) {
            throw new ResponseError('The specified entity set cannot be found', 409);
          }
          // save item by assigning agent
          return this._context.model(findEntitySet.Name).save(item).then(() => {
            Object.assign(this.request, {
              agent: {
                id: user.id,
                name: user.name
              }
            });
            // do refresh
            this.load(this.request.id);
            this._loadingService.hideLoading();
          });
        });
      }).catch((err) => {
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }
  }

  /**
   * Current user claims the action
   */
  public release() {
    if (this.request) {
      this._modalService.showDialog(
          this._translateService.instant('Requests.Edit.ReleaseRequest'),
          this._translateService.instant('Requests.Edit.ModalReleaseTitle'),
          DIALOG_BUTTONS.YesNo
      ).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        this._context.getMetadata().then((schema) => {
          return this._userService.getUser().then((user) => {
            const item = {
              id: this.request.id,
              agent: null
            };
            // get entity set
            const findEntitySet = schema.EntityContainer.EntitySet.find((entitySet) => {
              return entitySet.EntityType === this.request.additionalType;
            });
            if (findEntitySet == null) {
              throw new ResponseError('The specified entity set cannot be found', 409);
            }
            // save item by assigning agent
            return this._context.model(findEntitySet.Name).save(item).then((result) => {
              Object.assign(this.request, result);
              this.refresh.next(this.request);
              this._loadingService.hideLoading();
            });
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      });
    }
  }

  async hasDescendants() {
    const action = this.request;
    if (action == null) {
      return [];
    }
    const schema = await this._context.getMetadata();
    const findEntitySet = schema.EntityContainer.EntitySet.find((entitySet) => {
      return action.additionalType === entitySet.EntityType;
    });
    if (findEntitySet == null) {
      throw new Error('Entity set cannot be found');
    }
    // search function
    const findFunction = schema.Function.find((f) => {
      return f.Name === 'initiatorOf' &&
          f.Parameter[0] &&
          f.Parameter[0].Name === 'bindingParameter' &&
          f.Parameter[0].Type === action.additionalType;
    });
    if (findFunction == null) {
      return [];
    }
    return await this._context.model(`${findEntitySet.Name}/${action.id}/${findFunction.Name}`).getItems();
  }

  reject(sendMessage?: boolean) {
    if (this.request) {
      const title = this._translateService.instant('Requests.Edit.CancelRequest');
      const message = this._translateService.instant('Requests.Edit.ModalRejectTitle');
          this._translateService.instant('Requests.CancelPublishMessage');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        const item = {
          id: this.request.id,
          actionStatus: {
            alternateName: 'CancelledActionStatus'
          }
        };
        this._context.getMetadata().then((schema) => {
          // get entity set
          const entitySet = schema.EntityContainer.EntitySet.find((x) => x.EntityType === this.request.additionalType);
          return this._context.model(entitySet.Name).save(item).then((result) => {
            if (sendMessage) {
              // send message
              this.sendMessage().then(() => {
                // refresh data
                Object.assign(this.request, result);
                this.refresh.next(this.request);
                this._loadingService.hideLoading();
              }).catch((err) => {
                // refresh data
                Object.assign(this.request, result);
                this.refresh.next(this.request);
                // show error for failed message
                const errorTitle = this._translateService.instant('Modals.FailedSendMessageTitle');
                const errorMessage = this._translateService.instant('Modals.FailedSendMessageMessage');
                this._loadingService.hideLoading();
                return this._modalService.showErrorDialog(errorTitle, errorMessage);
              });
            } else {
              // refresh data
              Object.assign(this.request, result);
              this.refresh.next(this.request);
              this._loadingService.hideLoading();
            }
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      });
    }
  }

  /**
   * Accepts an action
   */
  async accept(sendMessage?: boolean) {
    // get actions derived from this action (if any)
    if (this.initiatorOf && this.initiatorOf.length > 0) {
      // get first item
      const item = this.initiatorOf[0];
      // get schema
      const schema = await this._context.getMetadata();
      // get entity set
      const entitySet = schema.EntityContainer.EntitySet.find((x) => {
        return item.additionalType === x.EntityType;
      });
      const urlTree = this._router.createUrlTree([
        {
          outlets: {
            modal: `edit/${entitySet.Name}/${item.id}`
          }
        }
      ], {
        relativeTo: this._route
      });
      return this._router.navigateByUrl(urlTree, {
        replaceUrl: true
      }).catch((err) => {
        console.error(err);
      });
    } else {
      return await this.acceptThis();
    }
  }

  private async acceptThis(sendMessage?: boolean) {
      const title = this._translateService.instant('Requests.Edit.AcceptRequest');
      const message = this._translateService.instant('Requests.Edit.ModalAcceptTitle');
      this._modalService.showDialog(title, message, DIALOG_BUTTONS.YesNo).then( dialogResult => {
        if (dialogResult === 'no') {
          return;
        }
        this._loadingService.showLoading();
        const item = {
          id: this.request.id,
          actionStatus: {
            alternateName: 'CompletedActionStatus'
          }
        };
        this._context.getMetadata().then((schema) => {
          // get entity set
          const entitySet = schema.EntityContainer.EntitySet.find((x) => x.EntityType === this.request.additionalType);
          return this._context.model(entitySet.Name).save(item).then((result) => {
            if (sendMessage) {
              this.sendMessage().then(() => {
                // do refresh
                Object.assign(this.request, result);
                this.load(this.request.id);
                // send application event
                this._appEvent.change.next({
                  model: this.request.additionalType,
                  target: this.request
                });
                this._loadingService.hideLoading();
              }).catch((err) => {
                Object.assign(this.request, result);
                this.load(this.request.id);
                this._loadingService.hideLoading();
                // show error for failed message
                const errorTitle = this._translateService.instant('Modals.FailedSendMessageTitle');
                const errorMessage = this._translateService.instant('Modals.FailedSendMessageMessage');
                this._loadingService.hideLoading();
                return this._modalService.showErrorDialog(errorTitle, errorMessage);
              });
            } else {
              // do refresh
              Object.assign(this.request, result);
              this.load(this.request.id);
              // send application event
              this._appEvent.change.next({
                model: entitySet.Name,
                target: this.request
              });
              this._loadingService.hideLoading();
            }
          });
        }).catch((err) => {
          this._loadingService.hideLoading();
          console.log(err);
          const errorTitle = this._translateService.instant('Requests.AcceptRequestFailed');
          return this._modalService.showWarningDialog(errorTitle, err.error ? err.error.message : err.message);
        });
      });
  }

  // loads general data
  load(id: any) {
    // initialize values
    this.responseModel.attachment = null;
    this.responseModel.body = null;
    this._loadingService.showLoading();
    Promise.all([
          this._userService.getUser(),
          this._requestsService.getRequestById(id)
        ]).then((results) => {
      // get user
      this.currentUser = results[0];
      // get request
      const res = results[1];
      if (res === undefined) {
        this._loadingService.hideLoading();
        return this._errorService.navigateToError(new ResponseError('The specified request cannot be found', 404));
      }
      this.responseModel.subject = this
        ._translate
        .instant(`Requests.MessageMailSubject`);
      this.request = res;
      this._router.navigate([
        {
          outlets: {
            as: this.request.additionalType
          }
        }
      ], {
        relativeTo: this._route,
        replaceUrl: true
      }).then(() => {
        // get descendants
        return this.hasDescendants().then((result) => {
          // set request descendants
          this.initiatorOf = result;
          // do refresh
          this.refresh.next(this.request);
          this._loadingService.hideLoading();
        });
      }).catch((err) => {
        console.error(err);
        this._loadingService.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
    }, (err) => {
      this._loadingService.hideLoading();
      return this._errorService.navigateToError(err);
    });
  }

  async sendAndComplete(): Promise<any> {

  }

  async sendAndCancel(): Promise<any> {
    await this.reject(true);
  }

  private async sendMessage(): Promise<any> {
    // send message
    await this._requestsService.sendResponse(
        this.request.student.id,
        this.request.id,
        this.responseModel
    ).toPromise();
    // get messages and refresh
    this.request.messages = await this._context.model(`StudentRequestActions/${this.request.id}/messages`)
        .asQueryable()
        .expand('attachments')
        .orderByDescending('dateCreated')
        .getItems();
    // clear form
    this.responseModel = {
      body: null,
      attachment: null,
      subject: null
    };
  }

  async sendMessageAction(): Promise<any> {
    try {
      this._loadingService.showLoading();
      // send message
      await this.sendMessage();
      this._toastService.show(
          this._translateService.instant('Requests.Edit.SendMessage'),
          this._translateService.instant('Requests.Edit.OperationCompleted')
      );
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  // download the document file
  downloadFile(attachments) {
    this._requestsService.downloadFile(attachments);
  }

  // get the selected file
  onFileChanged(_event) {
    this.responseModel.attachment = this.file.nativeElement.files[0];
  }

  // reset the file of html input element
  reset() {
    this.file.nativeElement.value = '';
    this.responseModel.attachment = null;
  }

}

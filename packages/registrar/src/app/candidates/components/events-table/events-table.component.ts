import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import * as EVENTS_LIST_CONFIG from './events-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {ActivatedTableService} from '@universis/ngx-tables';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService, UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-enrollment-events-table',
  templateUrl: './events-table.component.html'
})
export class EnrollmentEventsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  public showPublish = true;
  public showSign = true;
  public recordsTotal: any;

  public readonly config = EVENTS_LIST_CONFIG;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _translateService: TranslateService,
              private _userActivityService: UserActivityService,
              private _modalService: ModalService,
              private _context: AngularDataContext,
              private _toastService: ToastService,
              private _errorService: ErrorService) { }

  ngOnInit() {
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.table.fetch(true);
      }
    });
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }

      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }

      this._userActivityService.setItem({
        category: this._translateService.instant('Candidates.Title'),
        description: this._translateService.instant(this.table.config.title),
        url: window.location.hash.substring(1), // get the path after the hash
        dateCreated: new Date
      });
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  remove() {
    if (this.table && this.table.selected && this.table.selected.length) {
      const items = this.table.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Candidates.RemoveEventTitle'),
        this._translateService.instant('Candidates.RemoveEventMessage.title'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
          this._context.model('StudyProgramEnrollmentEvents').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Candidates.RemoveEventMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Candidates.RemoveEventMessage.one' : 'Candidates.RemoveEventMessage.many')
                , { value: items.length })
            );
            this.table.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
      });

    }
  }
}


import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import { StudentsSharedModule } from '../students/students.shared';
import * as DEFAULT_EVENTS_LIST from './components/events-table/default-events-table.config.json';
import {
  EnrollmentEventsTableSearchResolver,
  EnrollmentEventsDefaultTableConfigurationResolver,
  EnrollmentEventsTableConfigurationResolver
} from './components/events-table/events-table-config.resolver';
import {CandidatesTableComponent} from './components/candidates/candidates-table.component';
import {
  CandidatesTableConfigurationResolver,
  CandidatesTableSearchResolver
} from './components/candidates/candidates-table-config.resolver';
@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        StudentsSharedModule
    ],
    providers: [
        EnrollmentEventsDefaultTableConfigurationResolver,
      EnrollmentEventsTableSearchResolver,
      EnrollmentEventsTableConfigurationResolver,
      CandidatesTableComponent,
      CandidatesTableConfigurationResolver,
      CandidatesTableSearchResolver
    ]
})
export class CandidatesSharedModule implements OnInit {
  public static readonly DefaultCandidatesList = DEFAULT_EVENTS_LIST;
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch((err) => {
            console.error('An error occurred while loading candidates shared module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach((language) => {
            import(`./i18n/candidates.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }
}

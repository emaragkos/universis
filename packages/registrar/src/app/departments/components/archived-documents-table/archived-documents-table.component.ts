import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ViewEncapsulation,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableDataResult,
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import 'rxjs/add/observable/combineLatest';
import { ErrorService } from '@universis/common';

declare var $: any;

import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedFilterValueProvider} from '@universis/ngx-tables';

@Component({
  selector: 'app-archived-documents-table',
  templateUrl: './archived-documents-table.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ArchivedDocumentsTableComponent implements OnInit, OnDestroy {

  public recordsTotal: number;
  private routeSub: Subscription;
  public documentSeriesId: number;
  private dataSubscription: Subscription;
  @ViewChild('documents') documents: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _errorService: ErrorService,
    private _activatedTableService: ActivatedTableService,
    private _advancedFilterValueProvider: AdvancedFilterValueProvider
  ) {}

  ngOnInit() {

    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTableService.activeTable = this.documents;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.documents) {
        this.documents.config = data.documents;
        this.advancedSearch.getQuery().then( res => {
          this.documents.destroy();
          this.documents.query = res;
          this.advancedSearch.text = '';
          this.documents.fetch(false);
        });
      }
    });

    this.routeSub = this._activatedRoute.params.subscribe(
      (params) => {
        this.documentSeriesId = params['documentSeries'];
        this._advancedFilterValueProvider.values['documentSeries'] = this.documentSeriesId;
        this.documents.ngOnInit();
      },
      (err) => {
        this._errorService.showError(err);
      }
    );
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.routeSub) {
      this.routeSub.unsubscribe();
    }
  }
}

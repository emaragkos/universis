import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import {DashboardComponent} from './dashboard.component';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {CommonModule} from '@angular/common';
import {DashboardSharedModule} from './dashboard.shared';
import { StudentsSharedModule } from '../students/students.shared';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        DashboardRoutingModule,
        TranslateModule,
        DashboardSharedModule,
        StudentsSharedModule,
        RouterModule
    ],
    declarations: [DashboardComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class DashboardModule { }

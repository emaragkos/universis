import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {RouterModalOkCancel} from '@universis/common/routing';
import {ActivatedRoute, Router} from '@angular/router';
import {AppEventService} from '@universis/common';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFormComponent} from '@universis/forms';
import {TranslateService} from '@ngx-translate/core';
import {SelectCourseComponent} from '../../../../courses/components/select-course/select-course-component';
import {AngularDataContext} from '@themost/angular';
// tslint:disable-next-line:max-line-length
import * as DEFAULT_SELECTABLE_COURSES_LIST from '../../../../courses/components/courses-table/courses-table.config.selectableSimpleComplex.json';

@Component({
  selector: 'app-add-courses',
  templateUrl: './add-courses.component.html'
})
export class AddCoursesComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  public loading = false;
  public lastError;
  @Input() execute: Observable<any>;
  @Input() items: Array<any> = [];
  @Input() target: any;
  @Input() beforeExecute: any;
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;
  @ViewChild('selectComponent') selectComponent: SelectCourseComponent;
  formConfig: any;
  private formLoadSubscription: Subscription;
  private formChangeSubscription: Subscription;
  public hasSelected = false;
  public tableConfig = DEFAULT_SELECTABLE_COURSES_LIST;

  constructor(router: Router,
              activatedRoute: ActivatedRoute,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _appEvent: AppEventService,
              private _toastService: ToastService,
              private _loadingService: LoadingService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    this.okButtonDisabled = true;
  }

  ngOnInit() {
    this.formLoadSubscription = this.formComponent.form.formLoad.subscribe(() => {
      if (this.formComponent.form.config) {
        // find submit button
        const findButton = this.formComponent.form.form.components.find(component => {
          return component.type === 'button' && component.key === 'submit';
        });
        // hide button
        if (findButton) {
          (<any>findButton).hidden = true;
        }
      }
    });
    this.formChangeSubscription = this.formComponent.form.change.subscribe((event) => {
      if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
        // enable or disable button based on form status
        this.okButtonDisabled = (!event.isValid);
      }
    });
    // format form data
    if (this.items && this.items.length) {
      const first = this.items[0];
      this.formComponent.data = {
        semester: first.semester,
        units: first.units,
        ects: first.ects,
        courseType: first.courseType,
        coefficient: first.coefficient
      };
    } else {
      this.formComponent.data = {
        semester: 1,
        units: 1,
        ects: 1,
        courseType: null,
        coefficient: 1,
        specialization: this.target
      };
    }
  }

  cancel(): Promise<any> {
    if (this.loading) {
      return;
    }
    // close
    if (this._modalService.modalRef) {
      return  this._modalService.modalRef.hide();
    }
  }

  ngOnDestroy(): void {
    if (this.formLoadSubscription) {
      this.formLoadSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
        this.formChangeSubscription.unsubscribe();
      }
    }

    ok(): Promise<any> {
      try {
        return new Promise((resolve, reject) => {
        this.loading = false;
        this.lastError = null;
        // get form data
        const data = this.formComponent.form.formio.data;
        // set courses
        this.items = this.selectComponent.advancedTable.selected.map((item) => {
          return {
            studyProgramCourse: {
              course: item, // set course
              studyProgram : this.target.studyProgram // set study program
            }
          };
        });
        // assign form attributes
        this.items.forEach((item) => {
          Object.assign(item, data);
        });
        // execute add
        const executeSubscription = this.execute.subscribe((result) => {
          this.loading = false;
          executeSubscription.unsubscribe();
          // close modal
          if (this._modalService.modalRef) {
            this._modalService.modalRef.hide();
          }
          return resolve();
        }, (err) => {
          this.loading = false;
          // ensure that loading is hidden
          this._loadingService.hideLoading();
          // set last error
          this.lastError = err;
          return reject(err);
        });
      });
    } catch (err) {
      this.loading = false;
      this._loadingService.hideLoading();
      this.lastError = err;
    }
  }

  onChange($event: any) {

  }

}

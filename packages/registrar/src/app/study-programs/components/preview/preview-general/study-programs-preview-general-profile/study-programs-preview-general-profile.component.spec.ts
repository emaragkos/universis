import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsPreviewGeneralProfileComponent } from './study-programs-preview-general-profile.component';

describe('StudyProgramsPreviewGeneralProfileComponent', () => {
  let component: StudyProgramsPreviewGeneralProfileComponent;
  let fixture: ComponentFixture<StudyProgramsPreviewGeneralProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsPreviewGeneralProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsPreviewGeneralProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-dashboard-general',
  templateUrl: './instructors-dashboard-general.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardGeneralComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Instructors')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('status,country,workCountry,department($expand=organization),user')
      .getItem();


  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsDashboardOverviewProfileComponent } from './instructors-dashboard-overview-profile.component';

describe('InstructorsDashboardOverviewProfileComponent', () => {
  let component: InstructorsDashboardOverviewProfileComponent;
  let fixture: ComponentFixture<InstructorsDashboardOverviewProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorsDashboardOverviewProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsDashboardOverviewProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

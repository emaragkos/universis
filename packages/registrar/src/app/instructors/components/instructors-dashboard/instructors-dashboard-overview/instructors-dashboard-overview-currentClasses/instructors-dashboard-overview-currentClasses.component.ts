import { Component, OnInit, Input } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-dashboard-overview-currentclasses',
  templateUrl: './instructors-dashboard-overview-currentClasses.component.html',
  styleUrls: ['../../instructors-dashboard.component.scss']
})
export class InstructorsDashboardOverviewCurrentClassesComponent implements OnInit {
  @Input() id: any;
  public model: any;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }
  async ngOnInit() {
    this.model = await this._context.model('Instructors/' + this.id + '/currentClasses')
      .asQueryable()
      .expand('period,status,course($expand=department)')
      //   .take(5)
      .getItems();
  }
}

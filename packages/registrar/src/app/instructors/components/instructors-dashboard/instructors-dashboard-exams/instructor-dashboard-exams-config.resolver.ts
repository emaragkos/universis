import {TableConfiguration} from '@universis/ngx-tables';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class InstructorDashboardExamsConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-exams.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./instructors-dashboard-exams.config.list.json`);
        });
    }
}

export class InstructorDashboardExamsSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./instructors-dashboard-exams.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./instructors-dashboard-exams.search.list.json`);
            });
    }
}

export class InstructorDefaultDashboardExamsConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./instructors-dashboard-exams.config.list.json`);
    }
}
